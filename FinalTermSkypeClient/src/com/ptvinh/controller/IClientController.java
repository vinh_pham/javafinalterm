package com.ptvinh.controller;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

import com.ptvinh.model.Connector;
import com.ptvinh.model.MyFriend;
import com.ptvinh.model.MyMessage;

public interface IClientController {

	void sendResultLogin(Connector conn, byte[] b);

	void updateIncomingMessage(String message);

	void updateGUIRooms(ArrayList<String> roomNames);

	void updateListFriendInRoom(String userNameExcept, HashMap<String,MyFriend> listFriendsInRoom);



	void notifyMessageOnScreen(String roomName, JPanel panel_textmessage);

	void updateGUIListAvailableFriends(HashMap<String, MyFriend> listAvaiFriends);


	void updateGUIHaveFriendOnline(HashMap<String, MyFriend> listFriends);

	void sendResultCreateAccount(Connector connector, byte[] b);

	void notifyRoomNewNameFailed();

	void notifyRoomNewNameSuccess(String oldName, String newName);

	void updateGUIListFriendInRoomIfCan();

	void okexitWindow();

}
