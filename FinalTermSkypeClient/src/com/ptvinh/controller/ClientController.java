package com.ptvinh.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.JPanel;

import com.ptvinh.model.Connector;
import com.ptvinh.model.MyFriend;
import com.ptvinh.model.MyMember;
import com.ptvinh.model.MyMessage;
import com.ptvinh.view.IMainClient;

public class ClientController implements IClientController{

	private IMainClient iMainClient;
	private Connector conn;
	private MyMember mMember;
	private String mRoomPresent = null;
	public MyMember getmMember() {
		return mMember;
	}



	public void setmMember(MyMember mMember) {
		this.mMember = mMember;
	}



	private String Username, Password;

	public String getUsername() {
		return Username;
	}



	public void setUsername(String username) {
		Username = username;
	}



	public ClientController(IMainClient imainClient)	 {
		// TODO Auto-generated constructor stub
		this.iMainClient = imainClient;
		
	}

	

	public void connectToServer(String ipAddress, int port) {
		// TODO Auto-generated method stub
		conn = new Connector(ipAddress,port, this);
	}

	public void loginAccount(String username, String password) {
		// TODO Auto-generated method stub
		
		conn.sendAccount(username, password);
		conn.waitingResultLogin();
		this.Username = username;
		this.Password = password;
		
		
	}



	@Override
	public void sendResultLogin(Connector conn, byte[] b) { 
		// TODO Auto-generated method stub
		if(checkLoginSuccess(b))
		{
			// du lieu byte den se là list friend 
			this.iMainClient.loginSuccess();
			mMember = MyMember.createMember(this, this.Username, this.Password, conn);
			
			
			mMember.addListFriends(b);
			
			iMainClient.updateIncomingMessage(new String(b));

			iMainClient.updateGUIListFriend(this.mMember.getListFriends());
		}else
		{
			this.iMainClient.loginFailed();
		}
	}



	private boolean checkLoginSuccess(byte[] b) {
		// TODO Auto-generated method stub
		// Kiểm tra 3 byte dau tien of server trả về kết quả là SUC or FAI
		byte[] kq = new byte[3];
		kq[0] = b[0];
		kq[1] = b[1];
		kq[2] = b[2];
		String re = new String(kq);
		if(re.equals("FAI"))
			return false;
		return true;
		
	}


	public void loadAllRooms() {
		// TODO Auto-generated method stub
		this.mMember.getAllRooms();
	}



	@Override
	public void updateIncomingMessage(String message) {
		// TODO Auto-generated method stub
		this.iMainClient.updateIncomingMessage(message);
	}



	@Override
	public void updateGUIRooms(ArrayList<String> roomNames) {
		// TODO Auto-generated method stub
		this.iMainClient.updateListRooms(roomNames);
	}



	public void loadListFriendInRooms(String RoomName) {
		// TODO Auto-generated method stub
		this.mRoomPresent = RoomName;
		this.mMember.loadListFriendInRoom(RoomName);
		
		
		loadHistoryMessageInRoom(RoomName);
		//this.mMember.loadListMessageInRoom(RoomName);
		
	}

	@Override
	public void updateListFriendInRoom(String userNameExcept,HashMap<String, MyFriend> listFriends) {
		// TODO Auto-generated method stub
		ArrayList<MyFriend> listFriend = new ArrayList<>();
		for(Entry<String, MyFriend> entry: listFriends.entrySet())
		{
			MyFriend fr = entry.getValue();
			if(!fr.getmUserName().equals(userNameExcept))
				listFriend.add(fr);
		}
		this.iMainClient.updateListFriendInRoom(listFriend);
	}


	public void sendMessageToRoom(String RoomName, String message) {
		// TODO Auto-generated method stub
		this.mMember.addMessageToRoom(RoomName, message);
		this.conn.sendMessageToRoom(RoomName, message);
		
	}

	@Override
	public void notifyMessageOnScreen(String roomName, JPanel panel_textmessage) {
		// TODO Auto-generated method stub
		this.iMainClient.notifyMessageOnScreen(roomName, panel_textmessage);
	}

	public void getListAvailableFriends() {
		// TODO Auto-generated method stub
		this.mMember.getListAvailableFriends();
		
	}



	@Override
	public void updateGUIListAvailableFriends(HashMap<String, MyFriend> listAvaiFriends) {
		// TODO Auto-generated method stub
		this.iMainClient.updateGUIListAvailableFriends(listAvaiFriends, this.mMember.getListFriends());
	}



	



	@Override
	public void updateGUIHaveFriendOnline(HashMap<String, MyFriend> listFriends) {
		// TODO Auto-generated method stub
		iMainClient.updateGUIListFriend(listFriends);
	}



	public void createAccount(String username2, String password2) {
		// TODO Auto-generated method stub
		conn.sendCreateAccount(username2, password2);
		conn.waitingResultCreatingAccount();
		this.Username = username2;
		this.Password = password2;
	}



	@Override
	public void sendResultCreateAccount(Connector connector, byte[] b) {
		// TODO Auto-generated method stub
		if(checkCreatingSuccess(b))
		{
			this.iMainClient.createAccountSuccess();
			mMember = MyMember.createMember(this, this.Username, this.Password, conn);
			return;
		}
		this.iMainClient.createAccountFailed();
	}

	private boolean checkCreatingSuccess(byte[] b) {
		// TODO Auto-generated method stub
		// Kiểm tra 3 byte dau tien of server trả về kết quả là SUC or FAI
		byte[] kq = new byte[3];
		kq[0] = b[0];
		kq[1] = b[1];
		kq[2] = b[2];
		String re = new String(kq);
		if(re.equals("CAF"))
			return false;
		return true;
		
	}



	public ArrayList<String> getListRoomChatExcept(String userNameExcept) {
		// TODO Auto-generated method stub
		return this.mMember.getListRoomChatExcept(userNameExcept);
	}



	public void createRoomChat(String withUserName) {
		// TODO Auto-generated method stub
		this.mMember.createRoomChat(withUserName);
	}



	public void editRoomName(String room_oldname, String room_newname) {
		// TODO Auto-generated method stub
		if(!this.mMember.checkRoomNewName(room_newname)) // nếu ko có tên trong ds phòng
		{
			this.mMember.sendNewRoomNameToServer(room_oldname, room_newname);
		}else{
			this.iMainClient.changeRoomNameFailed();
		}
		
	}



	@Override
	public void notifyRoomNewNameFailed() {
		// TODO Auto-generated method stub
		this.iMainClient.changeRoomNameFailed();
	}



	@Override
	public void notifyRoomNewNameSuccess(String oldName, String newName) {
		// TODO Auto-generated method stub
		this.iMainClient.changeRoomNameSuccess(oldName, newName);
	}



	@Override
	public void updateGUIListFriendInRoomIfCan() {
		// TODO Auto-generated method stub
		if(this.mRoomPresent != null)
			this.loadListFriendInRooms(mRoomPresent);
	}



	public void loadHistoryMessageInRoom(String RoomName) {
		// TODO Auto-generated method stub
		this.mMember.getAllHistoryChatInRoom(RoomName);
	}



	public void exitWindow() {
		// TODO Auto-generated method stub
		this.mMember.signOut();
		
	}



	@Override
	public void okexitWindow() {
		// TODO Auto-generated method stub
		this.iMainClient.okExitWindows();
	}



	public void exitWindowNonLogin() {
		// TODO Auto-generated method stub
		this.conn.sendRequestClientNonSignIn();
	}



	public void sendFileToRoom(String RoomName, File file) {
		// TODO Auto-generated method stub
		this.mMember.sendFileToRoom(RoomName, file);
		
	}




}

