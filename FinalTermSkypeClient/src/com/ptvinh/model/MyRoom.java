package com.ptvinh.model;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import com.ptvinh.controller.IClientController;

public class MyRoom {
	String RoomName;
	HashMap<String, MyFriend> listFriendsInRoom;
	ArrayList<MyMessage> listMessage;
	JPanel panel_textmessage;
	

	public MyRoom(String RoomName, HashMap<String, MyFriend> listFriendsInRoom, ArrayList<MyMessage> listMessage) {
		this.RoomName = RoomName;
		this.listFriendsInRoom = listFriendsInRoom;
		this.listMessage = listMessage;		
	}
	
	public MyRoom(String roomName, HashMap<String, MyFriend> listFriend) {
		// TODO Auto-generated constructor stub
		this.RoomName = roomName;
		this.listFriendsInRoom = listFriend;
		this.listMessage = new ArrayList<MyMessage>();
		panel_textmessage = new JPanel();
		panel_textmessage.setLayout(new BoxLayout(panel_textmessage, BoxLayout.Y_AXIS));
	}
	

	public void setRoomName(String roomName) {
		RoomName = roomName;
	}

	
	public ArrayList<MyMessage> getListMessage() {
		return listMessage;
	}
	public void setListMessage(ArrayList<MyMessage> listMessage) {
		this.listMessage = listMessage;
	}

	public String getRoomName() {
		// TODO Auto-generated method stub
		return this.RoomName;
	}

	public void addMessage(IClientController iController, String userName, MyMessage message1) {
		// TODO Auto-generated method stub
		this.listMessage.add(message1);
		AddContentMessageToJPanel(iController, userName,message1);
	}

	private void AddContentMessageToJPanel(IClientController iController, String userName, MyMessage message) {
		// TODO Auto-generated method stub
		addMessageToPanelList(userName, message);
		iController.notifyMessageOnScreen(RoomName,panel_textmessage);
	}

	private void addMessageToPanelList(String userName, MyMessage message) {
		if(userName.equals(message.getmUserName()))
		{
			JLabel lblNewLabel = new JLabel(message.getmUserName());
			
			lblNewLabel.setForeground(new Color(0, 139, 139));
			lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
			
			
			JComponent label = new JLabel(message.getmContent());
			label .setForeground(new Color(112, 128, 144));
			label.setFont(new Font("Tahoma", Font.PLAIN, 12));
			
			panel_textmessage.add(lblNewLabel);
			panel_textmessage.add(label);
			
		}
		else
		{
			JLabel label_2 = new JLabel(message.getmUserName());
			label_2.setFont(new Font("Tahoma", Font.PLAIN, 14));
			label_2.setBackground(new Color(102, 205, 170));
			
			
			JComponent label_1 = new JLabel(message.getmContent());
			label_1.setForeground(new Color(255, 69, 0));
			label_1.setFont(new Font("Tahoma", Font.PLAIN, 12));
			
			panel_textmessage.add(label_2);
			panel_textmessage.add(label_1);
		}
	}

	public boolean containFriend(String userNameExcept) {
		// TODO Auto-generated method stub
		return this.listFriendsInRoom.containsKey(userNameExcept) ? true:false;
	}

	public void loadAllMessageOnScreen(IClientController iController) {
		// TODO Auto-generated method stub
		
		iController.notifyMessageOnScreen(RoomName,panel_textmessage);
	}

	public void receiveHistoryMessage(IClientController iController,String userName, ArrayList<MyMessage> messages) {
		// TODO Auto-generated method stub
		this.listMessage = messages;
		this.panel_textmessage.removeAll();
		for(MyMessage msg: this.listMessage)
		{
			this.addMessageToPanelList(userName, msg);
		}
		iController.notifyMessageOnScreen(RoomName,panel_textmessage);
	}
}
