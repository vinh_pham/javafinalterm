package com.ptvinh.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import com.ptvinh.controller.ClientController;
import com.ptvinh.controller.IClientController;

public class Connector {

	private Socket socketOfClient;
	private DataOutputStream out;
	private DataInputStream in;
	private boolean running = true;
	public IClientController iController;
	private IMyMember iMember;
	private FileOutputStream fos;
	private String roomName;
	private String fileName;
	private String fileSize;
	private MyMessage msg;
	
	public static final String KEY_ROOMNEWNAME = "RNN";
	public static final String KEY_VERIFYAC = "VAC";
	public static final String KEY_CREATEAC = "CAC";
	public static final String KEY_ALLROOM = "LAR";
	public static final String KEY_SENDMSG = "SMS";
	public static final String KEY_LISTAVAILABLEFRIEND = "AFS";
	public static final String KEY_HAVEFRIENDONLINE = "HFO";
	public static final String KEY_ADDNEWFRIEND = "ANF";
	public static final String KEY_ADDNEWFRIENDSUCCESS = "SNF";
	public static final String KEY_ADDNEWFRIENDFAILED = "FNF";
	public static final String KEY_CREATEROOMCHAT = "CRC";

	public static final String KEY_RoomNewNameFailed = "RNF";
	public static final String KEY_RoomNewNameSuccess = "RNS";
	public static final String KEY_HISTORYMESSAGEINROOM = "HAR";
	
	public static final String KEY_EXITWDW = "EXI";
	public static final String KEY_HAVEFRIENDOFFLINE = "HFF";
	public static final String KEY_SENDFILE = "SEF";
	protected static final int FileSize = 7 * 1000000;



	public Connector(String ipAddress, int port, IClientController iController) {
		// TODO Auto-generated constructor stub
		this.iController = iController;
		try {
			
			 socketOfClient = new Socket(ipAddress, port);			
			 OutputStream outToServer = socketOfClient.getOutputStream();
	         out = new DataOutputStream(outToServer);
	        
	         InputStream inFromServer = socketOfClient.getInputStream();
	         in = new DataInputStream(inFromServer);
	         
	         /*
	         out.writeUTF("Hello from "
                     + socketOfClient.getLocalSocketAddress());
	         
	         System.out.println("Server says " + in.readUTF());*/
	        
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			System.err.println("Don't know about host " + ipAddress);
			e.printStackTrace();
			return;
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for the connection to " + ipAddress);
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}
	public void sendAccount(String username, String password) {
		// TODO Auto-generated method stub
		try {
			String temp = KEY_VERIFYAC + username + "/" + password;
			this.out.write(temp.getBytes());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void waitingResultLogin() {
		// TODO Auto-generated method stub
		new Thread(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
            	try {
            		byte[] b = new byte[1024];
					int k = in.read(b);
					iController.sendResultLogin(Connector.this,b);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}.start();
		
	}
	private String ExitWindowFristBytes(byte a, byte b, byte c) {
		// TODO Auto-generated method stub
		// kiểm tra xem abc có mã là text hay hình ảnh. text = "TXT" hinh = "IMG"
		
		byte[] bs = new byte[3];
		bs[0] = a;
		bs[1] = b;
		bs[2] = c;
		String re = new String(bs);
		if(re.equals(Connector.KEY_EXITWDW))
		{
			return KEY_EXITWDW;
		}
		if(re.equals(Connector.KEY_SENDFILE))
		{
			return KEY_SENDFILE;
		}
		return "NULL";
	}
	// lắng nghe dữ liệu đến
	public void listenIncomingData() {
		// TODO Auto-generated method stub
		new Thread(){
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
				while(running)
				{
	            	try {
	            		byte[] b = new byte[65536];
						in.read(b);
						if(ExitWindowFristBytes(b[0],b[1],b[2]).equals(Connector.KEY_EXITWDW))
						{
							running = false;
							in.close();
							out.close();
						}
						else if(ExitWindowFristBytes(b[0],b[1],b[2]).equals(Connector.KEY_SENDFILE))
						{
							fileName="";
		            		roomName="";
		            		fileSize="";
		            		
		            		parseDataFile(b);
		            		
		            		b = new byte[Integer.valueOf(fileSize)];
		            		in.readFully(b);
		            		iMember.writeToFile(roomName,fileName,Integer.valueOf(fileSize),b, msg);
						}
						
						iMember.sendIncomingData(b);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}
		}.start();
		
	}
	
	protected void parseDataFile(byte[] b) {
		// TODO Auto-generated method stub
		String byteToStr = new String(b);
		 
	  	 byteToStr = byteToStr.substring(3);
	  	
	  	 String[] result = byteToStr.split("/");
	  	roomName = result[0];
	  	fileName = result[1];
	  	fileSize = result[2]; 
	  	msg = new MyMessage(result[3],result[4]);
	}
	public void getAllRooms() {
		// TODO Auto-generated method stub
		try {
			String temp = KEY_ALLROOM;
		
			this.out.write(temp.getBytes());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void setIMyMember(IMyMember iMember) {
		// TODO Auto-generated method stub
		this.iMember = iMember;
	}
	public void sendMessageToRoom(String roomName, String message) {
		// TODO Auto-generated method stub
		String dataMessage = Connector.KEY_SENDMSG + roomName +"/"+ message + "/";
		this.sendMessage(dataMessage.getBytes());
	}
	private void sendMessage(byte[] bytes) {
		// TODO Auto-generated method stub
		try {
			this.out.write(bytes);
			this.out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void sendRequestToGetListAvailableFriend() {
		// TODO Auto-generated method stub
		this.sendMessage(Connector.KEY_LISTAVAILABLEFRIEND.getBytes());
	}
	public void sendCreateAccount(String username, String password) {
		// TODO Auto-generated method stub
		try {
			String temp = Connector.KEY_CREATEAC + username + "/" + password+"/";
			this.out.write(temp.getBytes());
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void waitingResultCreatingAccount() {
		// TODO Auto-generated method stub
		new Thread(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
            	try {
            		byte[] b = new byte[10];
					int k = in.read(b);
					iController.sendResultCreateAccount(Connector.this,b);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}.start();
		
	}
	public void sendRequestAddingNewFriend(String userNameFriend) {
		// TODO Auto-generated method stub
		this.sendMessage((Connector.KEY_ADDNEWFRIEND+userNameFriend+"/").getBytes());
	}
	public void createRoomChat(String withUserName) {
		// TODO Auto-generated method stub
		sendMessage((Connector.KEY_CREATEROOMCHAT + withUserName + "/").getBytes());
	}
	public void sendNewRoomNameToServer(String room_oldname, String room_newname) {
		// TODO Auto-generated method stub
		sendMessage((Connector.KEY_ROOMNEWNAME + room_oldname + "/" +room_newname+"/").getBytes());
	}
	public void sendRequestAllMessageInRoom(String roomName) {
		// TODO Auto-generated method stub
		sendMessage((Connector.KEY_HISTORYMESSAGEINROOM + roomName + "/").getBytes());
	}
	public void sendRequestClientSignOut() {
		// TODO Auto-generated method stub
		sendMessage((Connector.KEY_EXITWDW + "/").getBytes());
	}
	public void sendRequestClientNonSignIn() {
		// TODO Auto-generated method stub
		sendMessage((this.KEY_EXITWDW + "/").getBytes());
	}
	public void sendFileToRoom(String roomName, File file) {
		// TODO Auto-generated method stub
		
		String code = this.KEY_SENDFILE + roomName + "/" + file.getName() + "/"+file.length()+"/";
		/*String code = this.KEY_SENDFILE;*/
		
		 
		 try {
			 if(file.length() > FileSize)
			 {
				 // cannot sent file max too large
			 }
			 else
			 {
				byte[] data = new byte[(int)file.length()];
				
				sendMessage((code).getBytes());
				
				FileInputStream fis;
				fis = new FileInputStream(file);
				int count = fis.read(data);
				fis.close();
				/*byte[] DATA = coupleArrayByte((code+count+"/").getBytes(), data, count);*/

				System.out.println("count: " + count + " filesize: " + file.length());

				 out.write(data, 0, data.length);
				 	 
				 /*getdataFile(DATA);
					int offset = 3 + FfileName.getBytes().length + FRoomName.getBytes().length + FsizeDatabyte.getBytes().length +3;
					byte[] dataXXX = TrimArrayByte(DATA, offset, Integer.parseInt(FsizeDatabyte));
					
					 File filex = new File("D:\\iconXXXX.jpg");
					 FileOutputStream fos;
					try {
						fos = new FileOutputStream(filex);
						fos.write(dataXXX);
						fos.close();	
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
			 }
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private static byte[] TrimArrayByte(byte[] b, int offset, int sizeDatabyte) {
		// TODO Auto-generated method stub
		byte[] res = new byte[sizeDatabyte];
		for(int i = 0; i < sizeDatabyte; i++)
		{
			res[i] = b[offset++];
		}
		return res;
	}
	private static String FsizeDatabyte;
	private static String FfileName;
	private static String FRoomName;
	private static void getdataFile(byte[] b) {
		// TODO Auto-generated method stub
		String byteToStr = new String(b);
		 
	  	 byteToStr = byteToStr.substring(3);
	  	
	  	 String[] result = byteToStr.split("/");
	  	FRoomName = result[0];
	  	FfileName = result[1];
	  	FsizeDatabyte  =result[2]; 
	  
	  	
	}
	private static byte[] coupleArrayByte(byte[] code, byte[] data, int count) {
		// TODO Auto-generated method stub
		byte[] re = new byte[code.length + count];
		int j = 0;
		for(int i = 0; i < code.length + count; i++)
		{
			
			if(i < code.length)
				re[i] = code[i];
			else
			{
				re[i] = data[j];
				j++;
			}
		}
		return re;
	}

	

	
	
}
