package com.ptvinh.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.ptvinh.controller.ClientController;
import com.ptvinh.controller.IClientController;

public class MyMember implements IMyMember{
	private static final String strPathFolder = "DataItems\\";
	String userName;
	String password;
	boolean status = true;
	
	Connector conn;
	static int CountMember = 0;
	HashMap<String,MyFriend> listFriends; // list friend của User;
	HashMap<String,MyRoom> listRooms;
	HashMap<String,MyFriend> listAvaiFriends;
	private IClientController iController; 
	
	public HashMap<String,MyRoom> getListRooms() {
		return listRooms;
	}

	public boolean isStatus() {
		return status;
	}


	public void setStatus(boolean status) {
		this.status = status;
	}


	public void setListRooms(HashMap<String,MyRoom> listRooms) {
		this.listRooms = listRooms;
	}


	


	private MyMember(String userName, String password, Connector conn) {
		// TODO Auto-generated constructor stub
		this.userName = userName;
		this.password = password;
		this.conn = conn;
		this.listFriends = new HashMap<String,MyFriend>();
		this.listRooms = new HashMap<String,MyRoom>();
		this.listAvaiFriends = new HashMap<String,MyFriend>();
	}


	public static MyMember createMember(IClientController iClientController, String userName, String Password, Connector conn)
	{
		if(CountMember == 0)
		{
			MyMember member = new MyMember(userName, Password, conn);
			member.setIController(iClientController);
			conn.setIMyMember(member);
			member.startIncomingDataListener();
			return member;
		}
		return null;
		
	}
	
	
	private void startIncomingDataListener() {
		// TODO Auto-generated method stub
		this.conn.listenIncomingData();
	}

	private void setIController(IClientController iClientController) {
		// TODO Auto-generated method stub
		this.iController = iClientController;
 		
	}

	public Connector getConn() {
		return conn;
	}
	public void setConn(Connector conn) {
		this.conn = conn;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}


	/*public boolean addListFriends(byte[] b) {
		// TODO Auto-generated method stub
		String result = new String(b);
		// thuat toán lấy chuỗi
		user2-true/user7-false/user5-false/user6-false/user3-true/user4-false/
		MyFriend friend1 = new MyFriend("user2",true);
		MyFriend friend2 = new MyFriend("user7",false);
		MyFriend friend3 = new MyFriend("user5",false);
		MyFriend friend4 = new MyFriend("user6",false);
		MyFriend friend5 = new MyFriend("user3",true);
		MyFriend friend6 = new MyFriend("user4",false);
		this.listFriends.add(friend1);
		this.listFriends.add(friend2);
		this.listFriends.add(friend3);
		this.listFriends.add(friend4);
		this.listFriends.add(friend5);
		this.listFriends.add(friend6);
		

		
		return true;
		
		
	}*/
	public boolean addListFriends(byte[] b) {
		// TODO Auto-generated method stub

		String byteToStr = new String(b);

		// thuat toán lấy chuỗi
		// bỏ 3 ký tự "LFS"
		String result = byteToStr.substring(3);
		
		// tách thành từng cụm ngăn cách bởi dấu "/"
		
		String[] listdata = result.split("/");
		for(int i = 0; i <listdata.length - 1; i++)
		{
			String[] str2 = listdata[i].split("-");
			String userName = str2[0];
			boolean status = Boolean.valueOf(str2[1]);
			MyFriend myFriend = new MyFriend(userName, status);
			
			this.listFriends.put(myFriend.getmUserName(),myFriend);
		}

		return true;
	}

	private String getRoomNameFromMessage(byte[] b) {
		String byteToStr = new String(b);
		 
		// bỏ 3 ký tự "SMS"
	  	byteToStr = byteToStr.substring(3);
	  	
	  	String[] result = byteToStr.split("/");
	  	 
	  	return result[0];
	}
	private String getUserNameFromMessage(byte[] b) {
		String byteToStr = new String(b);
		 
		// bỏ 3 ký tự "SMS"
	  	byteToStr = byteToStr.substring(3);
	  	
	  	String[] result = byteToStr.split("/");
	  	 
	  	return result[1];
	}
	private String getContentFromMessage(byte[] b) {
		String byteToStr = new String(b);
		 
		// bỏ 3 ký tự "SMS"
	  	byteToStr = byteToStr.substring(3);
	  	
	  	String[] result = byteToStr.split("/");
	  	 
	  	return result[2];
	}
	public void getAllRooms() {
		// TODO Auto-generated method stub
		this.conn.getAllRooms();
	}
	@Override
	public void sendIncomingData(byte[] b) {
		// TODO Auto-generated method stubs
		switch(checkFristBytes(b[0], b[1], b[2]))
		{
			case 0: // du lieu message
				String message = new String(b);
				
				MyRoom room = getRoomFromMessage(getRoomNameFromMessage(b));
				if(room != null)
				{
					MyMessage message1 = new MyMessage(getUserNameFromMessage(b), getContentFromMessage(b));
					room.addMessage(this.iController, this.userName, message1);
				}
				break;
			case 1: // du lieu hinh anh
				break;
				
			case 2: // du lieu Chatting
				break;
			
			/*case 3: // du lieu ListFriend
				System.out.println("Receive List Friends");
				this.mMember.addListFriends(b);
				iMainClient.updateIncomingMessage(new String(b));
				break;*/
				
			case 4: // du lieu Friend Online
				break;
			
			case 5: // du lieu Friend Offline
				break;
				
			case 6: // du lieu All room
				String message2 = new String(b);
				this.parseStringToMyRoom(b);
				
				//this.listRooms = getListRoomFromBytes(b);
				
				ArrayList<String> RoomNames = new ArrayList<>();
				RoomNames = this.getRoomNames();
				
				this.iController.updateGUIRooms(RoomNames);
				
				this.iController.updateIncomingMessage(message2);
				
				break;
			case 7: // du lieu List Available Friend
				this.parseStringToListAvailableFriends(b);
				this.iController.updateGUIListAvailableFriends(this.listAvaiFriends);
				break;
			case 8: // Có một bạn vừa online
				String userFOnline = getUserFriendOnline(b);
				if(IsExistFriend(userFOnline))
				{
					MyFriend fr = this.listFriends.get(userFOnline);
					fr.setmStatus(true);
					
					this.iController.updateGUIHaveFriendOnline(this.listFriends);
				}
				if(IsExistFriendInListAvailableFriends(userFOnline))
				{
					MyFriend fr = this.listAvaiFriends.get(userFOnline);
					fr.setmStatus(true);
					this.iController.updateGUIListAvailableFriends(this.listAvaiFriends);
				}
				this.iController.updateGUIListFriendInRoomIfCan();
				break;
			case 9: // Add New Friend Thanh Cong
				String UserNameNFriend = getuserNameNewFriend(b);
				if(this.listAvaiFriends.size() != 0)
				{
					MyFriend fr = this.listAvaiFriends.get(UserNameNFriend);	
					this.listAvaiFriends.remove(fr.mUserName);
				}
				
				
				this.addNewFriend(new MyFriend(UserNameNFriend, true));
				
				this.iController.updateGUIHaveFriendOnline(this.listFriends);
				this.iController.updateGUIListAvailableFriends(this.listAvaiFriends);
				
				break;
			case 10: // du lieu KEY_RoomNewNameFailed
				this.iController.notifyRoomNewNameFailed();
				break;
			case 11: // du lieu KEY_RoomNewNameSuccess
				String OldName = getRoomOldName(b);
				String NewName = getRoomNewName(b);		
				if(this.listRooms.size() != 0)
				{
					MyRoom roomx = this.listRooms.get(OldName);
					roomx.setRoomName(NewName);
					this.listRooms.remove(OldName);
					this.listRooms.put(NewName, roomx);
					this.iController.notifyRoomNewNameSuccess(OldName, NewName);
					
				}
				
				break;
			case 12: // du lieu message chat trong phong
				//"HARroomName/user1-content, user1-content,user1-content,user1-content,/"
				String RoomName = getRoomNameHistory(b);
				ArrayList<MyMessage> Messages = getListMessage(b);		
				
				MyRoom roomHis = this.listRooms.get(RoomName);
				roomHis.receiveHistoryMessage(this.iController,this.userName,Messages);				
				break;
				
				
			case 13: //  Exit window
				this.iController.okexitWindow();
				break;
			case 14: //  Có bạn offline
				String useroffline = getRoomNameHistory(b);
				
				if(IsExistFriend(useroffline))
				{
					MyFriend fr = this.listFriends.get(useroffline);
					fr.setmStatus(false);
					
					this.iController.updateGUIHaveFriendOnline(this.listFriends);
				}
				if(IsExistFriendInListAvailableFriends(useroffline))
				{
					MyFriend fr = this.listAvaiFriends.get(useroffline);
					fr.setmStatus(false);
					this.iController.updateGUIListAvailableFriends(this.listAvaiFriends);
				}
				this.iController.updateGUIListFriendInRoomIfCan();
				
				break;
		}
	}


	private HashMap<String, MyRoom> getListRoomFromBytes(byte[] b) {
		HashMap<String, MyRoom> result = new HashMap<String, MyRoom>();
		
		String byteToStr = new String(b);
		// bỏ 3 ký tự "LAR"
		byteToStr = byteToStr.substring(3);
		
		// tách cac chuoi ngan cach boi dau "/"
		String[] listRoom = byteToStr.split("/");
		for (int i = 0; i < listRoom.length; i++) {
			// voi moi room tach ra 3 phan, roomName, listUser, listMessage
			// ngan cach boi dau "+"
			String[] values = listRoom[i].split("+");
			
			// values[0] la roomName
			// values[1] la listUser
			// values[2] la listMessage
			
			// tao 3 bien luu thong tin phong
			String roomName;
			HashMap<String, MyFriend> listUsersInRoom = new HashMap<String, MyFriend>();
			ArrayList<MyMessage> listMessagesInRoom = new ArrayList<MyMessage>();
			
			// assign  roomName;
			roomName = values[0];
			
			// tach thong tin values[1] ra thanh danh sach cac user trong room
			// ngan boi dau ","
			String[] listUsers = values[1].split(",");
			// moi user co username va online status
			for (int j = 0; j < listUsers.length; i++) {
				// tach ra ngan cach boi dau "-"
				String[] userInfo = listUsers[i].split("-");
				
				String userName = userInfo[0];
				boolean status = Boolean.valueOf(userInfo[1]);
				
				// tao doi tuong MyFriend
				MyFriend myFriend = new MyFriend(userName, status);
				// add vao listUsersInRoom
				listUsersInRoom.put(userName, myFriend);
			}
			
			// tach thong tin value[2] ra thanh danh sach cac message trong room
			// ngan boi dau ","
			String[] listMessages = values[2].split(",");
			// moi message co sender va content
			for (int k = 0; k < listMessages.length; k++) {
				// tach ra ngan boi dau "-"
				String[] messInfo = listMessages[i].split("-");
				
				String sender = messInfo[0];
				String content = messInfo[1];
				
				// tao doi tuong MyMessage
				MyMessage myMessage = new MyMessage(sender, content);
				// add vao ArrayList listMessagesInRoom
				listMessagesInRoom.add(myMessage);
			}
			
			// tao 1 room gom 3 thong tin
			MyRoom room = new MyRoom(roomName, listUsersInRoom, listMessagesInRoom);
			// add vao HashMap Room
			result.put(roomName, room);
		}
		return result;
	}

	private ArrayList<MyMessage> getListMessage(byte[] b) {
		ArrayList<MyMessage> listMyMessage = new ArrayList<MyMessage>();
		String byteToStr = new String(b);
		// bỏ 3 ký tự "HAR"
		byteToStr = byteToStr.substring(3);
		
		String[] result =  byteToStr.split("/");
		if(!result[1].equals(""))
		{
		// trong chuoi result[1] tach ra ngan cach boi dau ","
		String[] listMessages = result[1].split(",");
		for (int i = 0; i < listMessages.length; i++) {
			// voi moi messages co sender va content, ngan cach boi dau "-"
			String[] message = listMessages[i].split("-");
			String sender = message[0];
			String content = message[1];
			
			listMyMessage.add(new MyMessage(sender, content));
		}}
		return listMyMessage;
	}

	private String getRoomNameHistory(byte[] b) {
		String byteToStr = new String(b);
		// bỏ 3 ký tự "HAR"
		byteToStr = byteToStr.substring(3);
		
		String[] result =  byteToStr.split("/");
		return result[0];
	}

	private String getRoomNewName(byte[] b) {
		// TODO Auto-generated method stub
		String byteToStr = new String(b);
		 
		 // bỏ 3 ký tự "SMS"
	  	 byteToStr = byteToStr.substring(3);
	  	
	  	 String[] result = byteToStr.split("/");
	  	 
		 return result[1];
	}
	private String getRoomOldName(byte[] b) {
		// TODO Auto-generated method stub
		String byteToStr = new String(b);
		 
		 // bỏ 3 ký tự "SMS"
	  	 byteToStr = byteToStr.substring(3);
	  	
	  	 String[] result = byteToStr.split("/");
	  	 
		 return result[0];
	}


	private void addNewFriend(MyFriend fr) {
		// TODO Auto-generated method stub
		this.listFriends.put(fr.mUserName, fr);
	}

	private String getuserNameNewFriend(byte[] b) {
		// TODO Auto-generated method stub
		String byteToStr = new String(b);
		 
	  	byteToStr = byteToStr.substring(3);
	  	String[] listdata = byteToStr.split("/");
		return listdata[0];
	}

	private boolean IsExistFriendInListAvailableFriends(String userFOnline) {
		// TODO Auto-generated method stub
		return this.listAvaiFriends.containsKey(userFOnline) ? true:false;
	}

	private boolean IsExistFriend(String userFOnline) {
		// TODO Auto-generated method stub
		return this.listFriends.containsKey(userFOnline) ? true:false;
	}

	public HashMap<String, MyFriend> getListFriends() {
		return listFriends;
	}

	public void setListFriends(HashMap<String, MyFriend> listFriends) {
		this.listFriends = listFriends;
	}

	public HashMap<String, MyFriend> getListAvaiFriends() {
		return listAvaiFriends;
	}

	public void setListAvaiFriends(HashMap<String, MyFriend> listAvaiFriends) {
		this.listAvaiFriends = listAvaiFriends;
	}

	private String getUserFriendOnline(byte[] b) {
		// TODO Auto-generated method stub
		String byteToStr = new String(b);
		 
		// bỏ 3 ký tự "SMS"
	  	byteToStr = byteToStr.substring(3);
	  	String[] listdata = byteToStr.split("/");
		return listdata[0];
	}

	private void parseStringToListAvailableFriends(byte[] b) {
		// TODO Auto-generated method stub
		String byteToStr = new String(b);

		// thuat toán lấy chuỗi
		// bỏ 3 ký tự "LFS"
		String result = byteToStr.substring(3);
		
		// tách thành từng cụm ngăn cách bởi dấu "/"
		
		String[] listdata = result.split("/");
		for(int i = 0; i <listdata.length - 1; i++)
		{
			String[] str2 = listdata[i].split("-");
			String userName = str2[0];
			boolean status = Boolean.valueOf(str2[1]);
			MyFriend myFriend = new MyFriend(userName, status);
			
			this.listAvaiFriends.put(myFriend.getmUserName(), myFriend);
		}

		
	}

	private MyRoom getRoomFromMessage(String roomNameFromMessage) {
		// TODO Auto-generated method stub
		if(IsExistRoom(roomNameFromMessage))
		{
			return this.listRooms.get(roomNameFromMessage);
		}
		return null;
	}

	private boolean IsExistRoom(String roomNameFromMessage) {
		// TODO Auto-generated method stub
		return this.listRooms.containsKey(roomNameFromMessage) ? true:false;
	}

	private ArrayList<String> getRoomNames() {
		// TODO Auto-generated method stub
		ArrayList<String> RoomNames = new ArrayList<>();
		
		for(Entry<String, MyRoom> entry : this.listRooms.entrySet()) {
		    
		    MyRoom value = entry.getValue();
		    RoomNames.add(value.getRoomName());
		    

		}
		return RoomNames;
	}

	private void parseStringToMyRoom(byte[] b) {
		// TODO Auto-generated method stub
		// chuyen doi du lieu thanh MyRoom vd: "LAR/Phong 2,user2-true,user3-true,user2-true/Phong 1,user2-true,user3-true,user2-true" 
		
		String byteToStr = new String(b);
		// cắt bỏ chuỗi "LAR/"
		byteToStr = byteToStr.substring(4);
		
		String roomID, user, roomName;
		boolean status;
		// cắt thành cụm ngăn bởi dấu "/"
		for (String str1: byteToStr.split("/")){
			// với mỗi cụm, tách ra bởi dấu ","
			String[] str2 = str1.split(",");
			
			roomName = str2[0];	// chuỗi đầu tiên là phòng
			
			// với mỗi phòng có danh sách list friend
			HashMap<String, MyFriend> listFriend = new HashMap<String, MyFriend>();
			for (int i = 1; i < str2.length; i++) {
				// với mỗi cụm còn lại sẽ bao gồm tên user và online status, ngăn bởi dấu "-"
				String[] str3 = str2[i].split("-");
				user = str3[0];
				status = Boolean.valueOf(str3[1]);
				
				// add vào list
				if(IsExistFriend(user))
				{
					MyFriend fr = this.listFriends.get(user);
					listFriend.put(fr.getmUserName(),fr);
				}				
			}
			
			MyRoom myRoom = new MyRoom(roomName, listFriend);
			this.listRooms.put(roomName, myRoom);	
		}
	}

	private int checkFristBytes(byte a, byte b, byte c) {
		// TODO Auto-generated method stub
		// kiểm tra xem abc có mã là text hay hình ảnh. text = "TXT" hinh = "IMG"
		
		byte[] bs = new byte[3];
		bs[0] = a;
		bs[1] = b;
		bs[2] = c;
		String re = new String(bs);
		
		if(re.equals("LFS"))
			return 3;
		else if(re.equals(Connector.KEY_SENDMSG))
			return 0;
		else if(re.equals(Connector.KEY_ALLROOM))
			return 6;
		else if(re.equals(Connector.KEY_LISTAVAILABLEFRIEND))
			return 7;
		else if(re.equals(Connector.KEY_HAVEFRIENDONLINE))
			return 8;
		else if(re.equals(Connector.KEY_ADDNEWFRIENDSUCCESS))
			return 9;
		else if(re.equals(Connector.KEY_RoomNewNameFailed))
			return 10;
		else if(re.equals(Connector.KEY_RoomNewNameSuccess))
			return 11;
		else if(re.equals(Connector.KEY_HISTORYMESSAGEINROOM))
			return 12;
		else if(re.equals(Connector.KEY_EXITWDW))
			return 13;
		else if(re.equals(Connector.KEY_HAVEFRIENDOFFLINE))
			return 14;
		return -1;
	}

	public void loadListFriendInRoom(String roomName) {
		// TODO Auto-generated method stub
		this.iController.updateListFriendInRoom(this.userName,getListFriendInRoom(roomName));
	}

	private HashMap<String,MyFriend> getListFriendInRoom(String roomName) {
		// TODO Auto-generated method stub
		MyRoom room = this.listRooms.get(roomName);
		
		return room.listFriendsInRoom;
	}

	public void addMessageToRoom(String roomName, String message) {
		// TODO Auto-generated method stub
		MyRoom room = getRoomFromMessage(roomName);
		if(room != null)
		{
			MyMessage message1 = new MyMessage(this.userName,message);
			room.addMessage(this.iController,this.userName,message1);
		}
	}

	public void getListAvailableFriends() {
		// TODO Auto-generated method stub
		this.conn.sendRequestToGetListAvailableFriend();
	}

	public void addNewFriend(String userNameFriend) {
		// TODO Auto-generated method stub
		this.conn.sendRequestAddingNewFriend(userNameFriend);
	}

	public ArrayList<String> getListRoomChatExcept(String userNameExcept) {
		// TODO Auto-generated method stub
		ArrayList<String> rooms = new ArrayList<String>();
		
		for(Entry<String, MyRoom> entry: this.listRooms.entrySet())
		{
			MyRoom room = entry.getValue();
			if(!room.containFriend(userNameExcept))
			{
				rooms.add(room.getRoomName());
			}
		}
		
		return rooms;
	}

	public void createRoomChat(String withUserName) {
		// TODO Auto-generated method stub
		this.conn.createRoomChat(withUserName);
		
	}

	public boolean checkRoomNewName(String room_newname) {
		// TODO Auto-generated method stub
		return this.listRooms.containsKey(room_newname) ? true: false;
	}

	public void sendNewRoomNameToServer(String room_oldname, String room_newname) {
		// TODO Auto-generated method stub
		this.conn.sendNewRoomNameToServer(room_oldname, room_newname);
	}

	public void loadListMessageInRoom(String roomName) {
		// TODO Auto-generated method stub
		this.listRooms.get(roomName).loadAllMessageOnScreen(this.iController);
		
	}

	public void getAllHistoryChatInRoom(String roomName) {
		// TODO Auto-generated method stub
		this.conn.sendRequestAllMessageInRoom(roomName);
	}

	public void signOut() {
		// TODO Auto-generated method stub
		this.conn.sendRequestClientSignOut();
		
	}

	public void sendFileToRoom(String roomName, File file) {
		// TODO Auto-generated method stub
		/*file = new File("DataItems\\testdataitem.txt");*/
		this.listRooms.get(roomName).addMessage(iController, this.userName, new MyMessage(this.userName,"Sent File " + file.getName()));
		this.conn.sendFileToRoom(roomName,file);
	}

	@Override
	public void writeToFile(String roomName, String fileName, Integer filesize, byte[] DataXXX, MyMessage msg) {
		// TODO Auto-generated method stub
		
		this.listRooms.get(roomName).addMessage(iController, this.userName, msg);
		writeFileToFolder(MyMember.strPathFolder + fileName,DataXXX);
		System.out.println("Write File: " + MyMember.strPathFolder + fileName);
	}

	private void writeFileToFolder(String path, byte[] dataXXX) {
		// TODO Auto-generated method stub
		File filex = new File(path);
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(filex);
			fos.write(dataXXX);
			fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
