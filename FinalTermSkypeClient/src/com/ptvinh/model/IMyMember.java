package com.ptvinh.model;

public interface IMyMember {

	void sendIncomingData(byte[] b);

	void writeToFile(String roomName, String fileName, Integer filesize, byte[] DataXXX, MyMessage msg);
	
}
