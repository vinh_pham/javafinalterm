package com.ptvinh.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import com.ptvinh.controller.ClientController;
import com.ptvinh.model.MyFriend;
import com.ptvinh.model.MyMessage;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import java.awt.CardLayout;
import java.awt.Font;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Color;
import javax.swing.JList;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.BoxLayout;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Panel;
import java.awt.Canvas;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;

public class MainClient extends JFrame implements IMainClient{

	


	private JPanel contentPane;
	private static ClientController clientController;
	private int port = 9999;
	private String ipAddress = "localhost";
	private JPanel panel_login;
	private JLabel lblUsername;
	private JTextArea txt_username;
	private JTextArea txt_password;
	private JLabel lb_status;
	private JButton btnNewButton;
	private JPanel panel_chat;
	private JLabel lblClientChat;
	private JPanel panel_infor;
	private JTabbedPane tabbedPane;
	private JPanel panel_chatting;
	private JLabel lb_chatvsFriends;
	private JScrollPane scrollPane;
	private JPanel panel;
	private JScrollPane scrollPane_1;
	private JTextArea txt_SendingMessage;
	private JButton but_Send;
	private JPanel panel_5;
	private JButton btnLoadHistoryMessageInRoom;
	private JButton but_SendImage;
	private JButton btnSendFile;
	private JPanel panel_6;
	private JLabel lb_userName;
	private JLabel lb_Status;
	private JPanel panel_7;
	private JMenuBar menuBar;
	private JMenu mnTools;
	private JPanel panel_rooms;
	private JPanel panel_listfriend;
	private JPanel panel_online;
	private JPanel panel_offline;
	private JScrollPane scrollPane_2;
	private JPanel panel_list_friend;
	private JScrollPane scrollPane_3;
	private JPanel panel_list_online;
	private JScrollPane scrollPane_4;
	private JPanel panel_list_offline;
	private JButton btnLoadAllRooms;
	private JScrollPane scrollPane_5;
	private JPanel panel_list_rooms;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	private JLabel lblNewLabel;
	private JScrollPane scrollPane_6;
	private JPanel panel_list_friend_in_room;
	private JPanel panel_textmessage;
	private JPanel panel_available;
	private JButton btnLoadAllAvaiable;
	private JScrollPane scrollPane_7;
	private JPanel panel_list_available_friends;
	
	private HashMap<String, JButton> listButFriends = new HashMap<>();
	private HashMap<String, JButton> listButFriendsOnline= new HashMap<>();
	private HashMap<String, JButton> listButFriendsOffline= new HashMap<>();
	private HashMap<String, JButton> listButAvailableFriends= new HashMap<>();
	private HashMap<String, JButton> listButRooms= new HashMap<>();
	private JLabel label_status_createac;
	private JMenu mnNewMenu;
	private JMenuItem mntmNewMenuItem;
	private JMenuItem mntmNewMenuItem_1;
	private static boolean IsLogin = false;
	private JButton btnOpenfolderfiles;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainClient frame = new MainClient();
					frame.setVisible(true);
					frame.addWindowListener(new java.awt.event.WindowAdapter() {

						@Override
						public void windowClosing(WindowEvent arg0) {
							// TODO Auto-generated method stub
							if(IsLogin)
								clientController.exitWindow();
							else
							{
								clientController.exitWindowNonLogin(); // chưa login được muốn thoát
								System.exit(0);
							}
						}
						
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	public MainClient() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 789, 499);
		setTitle("Client");
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new CardLayout(0, 0));
		
		panel_login = new JPanel();
		contentPane.add(panel_login, "name_247804207085105");
		panel_login.setLayout(null);
		
		lblUsername = new JLabel("Username");
		lblUsername.setFont(new Font("Dialog", Font.PLAIN, 19));
		lblUsername.setEnabled(true);
		lblUsername.setBounds(117, 114, 99, 25);
		panel_login.add(lblUsername);
		
		txt_username = new JTextArea();
		txt_username.setFont(new Font("Dialog", Font.PLAIN, 19));
		txt_username.setBounds(226, 114, 150, 26);
		panel_login.add(txt_username);
		
		txt_password = new JTextArea();
		txt_password.setFont(new Font("Dialog", Font.PLAIN, 19));
		txt_password.setBounds(226, 168, 150, 26);
		panel_login.add(txt_password);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Dialog", Font.PLAIN, 19));
		lblPassword.setEnabled(true);
		lblPassword.setBounds(117, 168, 99, 25);
		panel_login.add(lblPassword);
		
		lb_status = new JLabel("");
		lb_status.setHorizontalAlignment(SwingConstants.CENTER);
		lb_status.setFont(new Font("Dialog", Font.PLAIN, 19));
		lb_status.setEnabled(true);
		lb_status.setBounds(117, 223, 259, 25);
		panel_login.add(lb_status);
		
		btnNewButton = new JButton("Sign In");
		btnNewButton.setFont(new Font("Dialog", Font.PLAIN, 19));
		btnNewButton.setBounds(187, 272, 157, 38);
		panel_login.add(btnNewButton);
		
		btnNewButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				
				String username = txt_username.getText();
				String password = txt_password.getText();
				
				if(checkConditionLogin(username,password))
				{
					clientController.loginAccount(username, password);
				}
				else {
					/// notify user
					showAlertDialog("Đăng Nhập Thất Bại");
				}
			}
			
		});
		
		
		panel_chat = new JPanel();
		contentPane.add(panel_chat, "name_248156658428271");
		panel_chat.setLayout(new BorderLayout(0, 0));
		
		panel_7 = new JPanel();
		panel_chat.add(panel_7, BorderLayout.CENTER);
		panel_7.setLayout(new BorderLayout(0, 0));
		
		lblClientChat = new JLabel("Client Chat");
		panel_7.add(lblClientChat, BorderLayout.NORTH);
		lblClientChat.setHorizontalAlignment(SwingConstants.CENTER);
		lblClientChat.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 17));
		
		panel_infor = new JPanel();
		panel_7.add(panel_infor, BorderLayout.WEST);
		panel_infor.setLayout(new BorderLayout(0, 0));
		
		panel_6 = new JPanel();
		panel_infor.add(panel_6, BorderLayout.NORTH);
		panel_6.setToolTipText("");
		GridBagLayout gbl_panel_6 = new GridBagLayout();
		gbl_panel_6.columnWidths = new int[] {260, 30, 30};
		gbl_panel_6.rowHeights = new int[] {20, 0, 2};
		gbl_panel_6.columnWeights = new double[]{0.0, 0.0};
		gbl_panel_6.rowWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		panel_6.setLayout(gbl_panel_6);
		
		lb_userName = new JLabel("UserName: ");
		GridBagConstraints gbc_lb_userName = new GridBagConstraints();
		gbc_lb_userName.anchor = GridBagConstraints.WEST;
		gbc_lb_userName.insets = new Insets(0, 0, 5, 5);
		gbc_lb_userName.gridx = 0;
		gbc_lb_userName.gridy = 0;
		panel_6.add(lb_userName, gbc_lb_userName);
		lb_userName.setHorizontalAlignment(SwingConstants.LEFT);
		lb_userName.setForeground(Color.RED);
		lb_userName.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));
		lb_userName.setBackground(Color.ORANGE);
		
		lb_Status = new JLabel("Status ");
		GridBagConstraints gbc_lb_Status = new GridBagConstraints();
		gbc_lb_Status.anchor = GridBagConstraints.WEST;
		gbc_lb_Status.insets = new Insets(0, 0, 0, 5);
		gbc_lb_Status.gridx = 0;
		gbc_lb_Status.gridy = 1;
		panel_6.add(lb_Status, gbc_lb_Status);
		lb_Status.setHorizontalAlignment(SwingConstants.LEFT);
		lb_Status.setForeground(Color.RED);
		lb_Status.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));
		lb_Status.setBackground(Color.ORANGE);
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		panel_infor.add(tabbedPane, BorderLayout.CENTER);
		
		panel_listfriend = new JPanel();
		tabbedPane.addTab("List Friends", null, panel_listfriend, null);
		panel_listfriend.setLayout(new BoxLayout(panel_listfriend, BoxLayout.PAGE_AXIS));
		
		scrollPane_2 = new JScrollPane();
		scrollPane_2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		panel_listfriend.add(scrollPane_2);
		
		panel_list_friend = new JPanel();
		scrollPane_2.setViewportView(panel_list_friend);
		panel_list_friend.setLayout(new BoxLayout(panel_list_friend, BoxLayout.PAGE_AXIS));
		
		panel_online = new JPanel();
		tabbedPane.addTab("Online", null, panel_online, null);
		panel_online.setLayout(new BoxLayout(panel_online, BoxLayout.X_AXIS));
		
		scrollPane_3 = new JScrollPane();
		scrollPane_3.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		panel_online.add(scrollPane_3);
		
		panel_list_online = new JPanel();
		scrollPane_3.setViewportView(panel_list_online);
		panel_list_online.setLayout(new BoxLayout(panel_list_online, BoxLayout.PAGE_AXIS));
		
		panel_offline = new JPanel();
		tabbedPane.addTab("Offline", null, panel_offline, null);
		panel_offline.setLayout(new BoxLayout(panel_offline, BoxLayout.X_AXIS));
		
		scrollPane_4 = new JScrollPane();
		scrollPane_4.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		panel_offline.add(scrollPane_4);
		
		panel_list_offline = new JPanel();
		scrollPane_4.setViewportView(panel_list_offline);
		panel_list_offline.setLayout(new BoxLayout(panel_list_offline, BoxLayout.PAGE_AXIS));
		
		panel_rooms = new JPanel();
		tabbedPane.addTab("Rooms", null, panel_rooms, null);
		panel_rooms.setLayout(new BorderLayout(0, 0));
		
		btnLoadAllRooms = new JButton("Load All Rooms");
		btnLoadAllRooms.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				clientController.loadAllRooms();
				btnLoadAllRooms.setEnabled(false);
			}
			
		});
		panel_rooms.add(btnLoadAllRooms, BorderLayout.NORTH);
		
		scrollPane_5 = new JScrollPane();
		panel_rooms.add(scrollPane_5, BorderLayout.CENTER);
		
		panel_list_rooms = new JPanel();
		scrollPane_5.setViewportView(panel_list_rooms);
		panel_list_rooms.setLayout(new BoxLayout(panel_list_rooms, BoxLayout.Y_AXIS));
		
		panel_available = new JPanel();
		tabbedPane.addTab("List Available Friends", null, panel_available, null);
		panel_available.setLayout(new BorderLayout(0, 0));
		
		btnLoadAllAvaiable = new JButton("Load All Avaiable Friends");
		btnLoadAllAvaiable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				clientController.getListAvailableFriends();
			}
		});
		panel_available.add(btnLoadAllAvaiable, BorderLayout.NORTH);
		
		scrollPane_7 = new JScrollPane();
		panel_available.add(scrollPane_7, BorderLayout.CENTER);
		
		panel_list_available_friends = new JPanel();
		scrollPane_7.setViewportView(panel_list_available_friends);
		panel_list_available_friends.setLayout(new BoxLayout(panel_list_available_friends, BoxLayout.Y_AXIS));
		
		panel_chatting = new JPanel();
		panel_7.add(panel_chatting, BorderLayout.CENTER);
		panel_chatting.setLayout(new BorderLayout(0, 0));
		
		lb_chatvsFriends = new JLabel("Communication");
		lb_chatvsFriends.setHorizontalAlignment(SwingConstants.CENTER);
		lb_chatvsFriends.setForeground(Color.RED);
		lb_chatvsFriends.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));
		lb_chatvsFriends.setBackground(Color.ORANGE);
		panel_chatting.add(lb_chatvsFriends, BorderLayout.NORTH);
		
		panel_1 = new JPanel();
		panel_chatting.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		scrollPane = new JScrollPane();
		panel_1.add(scrollPane, BorderLayout.CENTER);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		panel_textmessage = new JPanel();
		scrollPane.setViewportView(panel_textmessage);
		panel_textmessage.setLayout(new BoxLayout(panel_textmessage, BoxLayout.Y_AXIS));
		
		panel_2 = new JPanel();
		panel_1.add(panel_2, BorderLayout.EAST);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		lblNewLabel = new JLabel("Friends in Room");
		panel_2.add(lblNewLabel, BorderLayout.NORTH);
		
		panel_3 = new JPanel();
		panel_2.add(panel_3, BorderLayout.CENTER);
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.Y_AXIS));
		
		scrollPane_6 = new JScrollPane();
		panel_3.add(scrollPane_6);
		
		panel_list_friend_in_room = new JPanel();
		scrollPane_6.setViewportView(panel_list_friend_in_room);
		panel_list_friend_in_room.setLayout(new BoxLayout(panel_list_friend_in_room, BoxLayout.Y_AXIS));
		
		panel = new JPanel();
		panel_chatting.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new BorderLayout(0, 0));
		
		panel_5 = new JPanel();
		panel.add(panel_5, BorderLayout.NORTH);
		panel_5.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		btnLoadHistoryMessageInRoom = new JButton("Load History Message In Room");
		btnLoadHistoryMessageInRoom.setEnabled(false);
		btnLoadHistoryMessageInRoom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(isClickedRoom())
				{
					
					clientController.loadHistoryMessageInRoom(lb_chatvsFriends.getText());
				}
			}

			
		});
		btnLoadHistoryMessageInRoom.setHorizontalAlignment(SwingConstants.LEFT);
		panel_5.add(btnLoadHistoryMessageInRoom);
		
		but_SendImage = new JButton("Send Image");
		but_SendImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				final JFileChooser  fileDialog = new JFileChooser();
				FileFilter filter = new FileNameExtensionFilter("Image File","png", "jpg", "bitmap");
				fileDialog.addChoosableFileFilter(filter);
				fileDialog.setFileFilter(filter);
				int returnVal = fileDialog.showOpenDialog(MainClient.this);
	            if (returnVal == JFileChooser.APPROVE_OPTION) {
	               java.io.File file = fileDialog.getSelectedFile();
	               clientController.sendFileToRoom(lb_chatvsFriends.getText(),file);
	            }
			}
		});
		but_SendImage.setEnabled(false);
		but_SendImage.setHorizontalAlignment(SwingConstants.LEFT);
		panel_5.add(but_SendImage);
		
		btnSendFile = new JButton("Send File");
		btnSendFile.setEnabled(false);
		btnSendFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				final JFileChooser  fileDialog = new JFileChooser();
				int returnVal = fileDialog.showOpenDialog(MainClient.this);
	            if (returnVal == JFileChooser.APPROVE_OPTION) {
	               java.io.File file = fileDialog.getSelectedFile();
	               clientController.sendFileToRoom(lb_chatvsFriends.getText(),file);
	            }
			}
		});
		btnSendFile.setHorizontalAlignment(SwingConstants.LEFT);
		panel_5.add(btnSendFile);
		
		btnOpenfolderfiles = new JButton("OpenFolderFiles");
		btnOpenfolderfiles.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Desktop d= null;
				File file = new File("DataItems");
				if(Desktop.isDesktopSupported())
				{
					d = Desktop.getDesktop();
					try {
						d.open(file);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		btnOpenfolderfiles.setHorizontalAlignment(SwingConstants.LEFT);
		panel_5.add(btnOpenfolderfiles);
		
		but_Send = new JButton("Send");
		but_Send.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(isClickedRoom())
				{
					String message = txt_SendingMessage.getText();
					if(checkMessage(message)) // kiem tra message trước khi gởi di
					{
						clientController.sendMessageToRoom(lb_chatvsFriends.getText(),message);
						
					}
					else showAlertDialog("Kiểm tra lại tin nhắn nhé!");
				}
				else
					showAlertDialog("Kiểm tra lại chọn phòng gởi tin nhắn nhé! ");
			}
		});
		panel.add(but_Send, BorderLayout.EAST);
		
		scrollPane_1 = new JScrollPane();
		scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		panel.add(scrollPane_1);
		
		txt_SendingMessage = new JTextArea();
		txt_SendingMessage.setRows(4);
		scrollPane_1.setViewportView(txt_SendingMessage);
		
		menuBar = new JMenuBar();
		panel_chat.add(menuBar, BorderLayout.NORTH);
		
		mnTools = new JMenu("Tools");
		menuBar.add(mnTools);
		
		mnNewMenu = new JMenu("New menu");
		mnTools.add(mnNewMenu);
		
		mntmNewMenuItem_1 = new JMenuItem("New menu item");
		mnNewMenu.add(mntmNewMenuItem_1);
		
		mntmNewMenuItem = new JMenuItem("New menu item");
		mnNewMenu.add(mntmNewMenuItem);
		
		panel_chat.setVisible(false);
		
		clientController = new ClientController(this);
		clientController.connectToServer(ipAddress, port);
		
		txt_username.setText("user1");
		txt_password.setText("user1");
		
		JLabel lblSignIn = new JLabel("Sign In");
		lblSignIn.setFont(new Font("Tahoma", Font.PLAIN, 33));
		lblSignIn.setBounds(167, 34, 111, 57);
		panel_login.add(lblSignIn);
		
		JLabel lblSignUp = new JLabel("Sign Up");
		lblSignUp.setFont(new Font("Tahoma", Font.PLAIN, 33));
		lblSignUp.setBounds(502, 34, 128, 57);
		panel_login.add(lblSignUp);
		
		JLabel label_1 = new JLabel("Username");
		label_1.setFont(new Font("Dialog", Font.PLAIN, 19));
		label_1.setEnabled(true);
		label_1.setBounds(452, 114, 99, 25);
		panel_login.add(label_1);
		
		JLabel label_2 = new JLabel("Password");
		label_2.setFont(new Font("Dialog", Font.PLAIN, 19));
		label_2.setEnabled(true);
		label_2.setBounds(452, 168, 99, 25);
		panel_login.add(label_2);
		
		JTextArea txt_password_signup = new JTextArea();
		txt_password_signup.setFont(new Font("Dialog", Font.PLAIN, 19));
		txt_password_signup.setBounds(561, 168, 150, 26);
		panel_login.add(txt_password_signup);
		
		JTextArea txt_username_signup = new JTextArea();
		txt_username_signup.setFont(new Font("Dialog", Font.PLAIN, 19));
		txt_username_signup.setBounds(561, 114, 150, 26);
		panel_login.add(txt_username_signup);
		
		JButton btnSignUp = new JButton("Sign Up");
		btnSignUp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String username = txt_username_signup.getText();
				String password = txt_password_signup.getText();
				
				if(checkConditionLogin(username,password))
				{
					clientController.createAccount(username, password);
				}
				else {
					/// notify user
					showAlertDialog("Đăng Ký Thất Bại");
				}
			}
		});
		btnSignUp.setFont(new Font("Dialog", Font.PLAIN, 19));
		btnSignUp.setBounds(522, 272, 157, 38);
		panel_login.add(btnSignUp);
		
		label_status_createac = new JLabel("");
		label_status_createac.setHorizontalAlignment(SwingConstants.CENTER);
		label_status_createac.setFont(new Font("Dialog", Font.PLAIN, 19));
		label_status_createac.setEnabled(true);
		label_status_createac.setBounds(452, 223, 259, 25);
		panel_login.add(label_status_createac);
	}
	
	

	
	protected void showAlertDialog(String message) {
		// TODO Auto-generated method stub
		JOptionPane optionPane = new JOptionPane(message,JOptionPane.WARNING_MESSAGE);
		JDialog dialog = optionPane.createDialog("Thông Báo");
		dialog.setAlwaysOnTop(true); // to show top of all other application
		dialog.setVisible(true); // to visible the dialog
	}


	protected boolean checkMessage(String message) {
		// TODO Auto-generated method stub
		return true;
	}


	private boolean checkConditionLogin(String userName, String password) {
		// TODO Auto-generated method stub
		return true;
	}
	private boolean isClickedRoom() {
		// TODO Auto-generated method stub
		if(lb_chatvsFriends.getText().equals("Communication"))
			return false;
		return true;
	}

	@Override
	public void loginSuccess() {
		// TODO Auto-generated method stub
		IsLogin  = true;
		this.lb_status.setText("Login Success !!!");
		SwingUtilities.invokeLater( 
	            new Runnable() {
	                public void run() {
	                	for(int i = 0; i < 1; i++)
	                	{
	                		try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	                	}
	                	MainClient.this.setVisible(false);
	                	MainClient.this.setBounds(100, 100, 1024, 720);
	                	MainClient.this.setVisible(true);
	                	MainClient.this.setTitle(clientController.getmMember().getUserName());
	                	panel_login.setVisible(false);
	                	panel_chat.setVisible(true);
	                	lb_userName.setText("Username: " + clientController.getmMember().getUserName());
	                	if(clientController.getmMember().isStatus())
	                		lb_Status.setText("Status: " +"Online");
	                	
	                }
	            }
	        );
		
	}

	@Override
	public void loginFailed() {
		// TODO Auto-generated method stub
		this.lb_status.setText("Login Failed !!!");
	}


	@Override
	public void updateIncomingMessage(String message) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater( 
	            new Runnable() {
	                public void run() {
	                	/*txt_Message.append(message+"\r\n");*/
	                }
	            }
	        );
	}

	
	
	
	@Override
	public void updateGUIListFriend(HashMap<String,MyFriend> listFriends) {
		// TODO Auto-generated method stub
		
		
		SwingUtilities.invokeLater( 
	            new Runnable() {
	                public void run() {
	                	panel_list_offline.removeAll();
        				panel_list_offline.repaint();
        				panel_list_online.removeAll();
        				panel_list_online.repaint();

        				panel_list_friend.removeAll();
        				panel_list_friend.repaint();
        				
	                	
	                	for(Entry<String, MyFriend> entry: listFriends.entrySet())
	            		{
	                		MyFriend fr = entry.getValue();
	                		JPopupMenu popup;

	                	    //...where the GUI is constructed:
	                	    //Create the popup menu.
	                	    popup = new JPopupMenu();
	                	    
	                	   

	                	    //Add listener to components that can bring up popup menus.
	                	    MouseListener popupListener = new PopupInMyListFriendListener(popup);
	                	    
	                		
	            			JButton buttonx = new JButton(fr.getmUserName());
	            			buttonx.setAlignmentX(Component.CENTER_ALIGNMENT);
	            			buttonx.setFont(new Font("Tahoma", Font.PLAIN, 19));
	            			if(fr.ismStatus())
	            			{
	            				buttonx.setForeground(new Color(0, 128, 0));
	            				JButton buttonOnline = new JButton(fr.getmUserName());
	            				buttonOnline.setAlignmentX(Component.CENTER_ALIGNMENT);
	            				buttonOnline.setFont(new Font("Tahoma", Font.PLAIN, 19));
	            				buttonOnline.setForeground(new Color(0, 128, 0));
	            				
	            				
	            				listButFriendsOnline.put(fr.getmUserName(), buttonOnline);
	            				panel_list_online.add(buttonOnline);  
	            				
	            				
	            			}
	            			else{
	            				buttonx.setForeground(Color.RED);
	            				JButton buttonOffline = new JButton(fr.getmUserName());
	            				buttonOffline.setAlignmentX(Component.CENTER_ALIGNMENT);
	            				buttonOffline.setFont(new Font("Tahoma", Font.PLAIN, 19));
	            				buttonOffline.setForeground(Color.RED);
	            				
	            				
	            				listButFriendsOffline.put(fr.getmUserName(), buttonOffline);
	            				panel_list_offline.add(buttonOffline);
	            			}
	            			
	            			
	            			listButFriends.put(fr.getmUserName(), buttonx);
	            			buttonx.addMouseListener(popupListener);

	            			panel_list_friend.add(buttonx);
	            		}
	                	panel_list_offline.revalidate();
        				panel_list_offline.repaint();
        				panel_list_online.revalidate();
        				panel_list_online.repaint();

        				panel_list_friend.revalidate();
        				panel_list_friend.repaint();
	                }
	            }
	        );
		
	}


	@Override
	public void updateListRooms(ArrayList<String> roomNames) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater( 
	            new Runnable() {
	                public void run() {
	                	panel_list_rooms.removeAll();
	                	panel_list_rooms.repaint();
	                	for(int i = 0; i < roomNames.size(); i++)
	            		{
	                		JPopupMenu popup;

	                	    //...where the GUI is constructed:
	                	    //Create the popup menu.
	                	    popup = new JPopupMenu();
	                	    
	                	  

	                	    //Add listener to components that can bring up popup menus.
	                	    MouseListener popupListRoomListener = new PopupListRoomListener(popup);
	                	    
	                	    menuBar.addMouseListener(popupListRoomListener);
	                		
	            			JButton buttonx = new JButton(roomNames.get(i));
	            			buttonx.setAlignmentX(Component.CENTER_ALIGNMENT);
	            			buttonx.setFont(new Font("Tahoma", Font.PLAIN, 19));
	            			
	            			
	            			buttonx.addMouseListener(popupListRoomListener);

	            			listButRooms.put(roomNames.get(i), buttonx);
	            			
	            			panel_list_rooms.add(buttonx);
	            			
	            		}
	                	panel_list_rooms.revalidate();
	        			panel_list_rooms.repaint();
	                }
	            }
	        );
	}
	
	class PopupListener extends MouseAdapter {
		
	    private JPopupMenu popup;
	    private boolean f =true;

	    
		public PopupListener(JPopupMenu popup) {
			this.popup = popup;
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			JButton but = (JButton) e.getComponent();
			System.out.println("Clicked " + but.getText());
			lb_chatvsFriends.setText(but.getText());
			
			
		}

		public void mousePressed(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    public void mouseReleased(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    private boolean maybeShowPopup(MouseEvent e) {
	        if (e.isPopupTrigger()) {
	        	
	        	if(f)
	        	{
		        	JMenuItem menuItem = new JMenuItem("Add Friend");
		        	menuItem.addActionListener(new ActionListener(){
	
						@Override
						public void actionPerformed(ActionEvent arg0) {
							// TODO Auto-generated method stub
							JButton but = (JButton) e.getComponent();
							System.out.println("Add Friend: " + but.getText());
							clientController.getmMember().addNewFriend(but.getText());
						}
		        		
		        	});
		        	popup.add(menuItem);
		        	f = false;
	        	}
        	    
        	    
	            popup.show(e.getComponent(),
	                       e.getX(), e.getY());
	            return true;
	        }
	        return false;
	    }
	}
	class PopupListRoomListener extends MouseAdapter {
		
	    private JPopupMenu popup;
		private boolean f = true;

	    
		public PopupListRoomListener(JPopupMenu popup) {
			this.popup = popup;
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			JButton but = (JButton) e.getComponent();
			System.out.println("Room Clicked " + but.getText());
			lb_chatvsFriends.setText(but.getText());
			btnLoadHistoryMessageInRoom.setEnabled(true);
			btnSendFile.setEnabled(true);
			but_SendImage.setEnabled(true);
			
			clientController.loadListFriendInRooms(but.getText());
			
		}

		public void mousePressed(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    public void mouseReleased(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    private boolean maybeShowPopup(MouseEvent e) {
	        if (e.isPopupTrigger()) {
	        	JButton but = (JButton) e.getComponent();
				
	        	if(f )
	        	{
	        	  JMenuItem menuItem = new JMenuItem("Edit room name");
	        	  menuItem.addActionListener(new ActionListener(){

					@Override
					public void actionPerformed(ActionEvent arg0) {
						// TODO Auto-generated method stub
						System.out.println("Edit room name: " + but.getText());
						
						String result = (String) JOptionPane.showInputDialog(MainClient.this,
						    "Room Name: " + but.getText() + "\r\n" + "Change: ",
						    "Edit Room Name" ,
						    JOptionPane.PLAIN_MESSAGE,
						    null,    
						    null,
						    but.getText());
						clientController.editRoomName(but.getText(), result);
						
					}					
	        	  });
            	    
            	    popup.add(menuItem);
            	    f = false; 
	        	}
	            popup.show(e.getComponent(),
	                       e.getX(), e.getY());
	            return true;
	        }
	        return false;
	    }
	}
	class PopupInMyListFriendListener extends MouseAdapter{
		private JPopupMenu popup;
		private JMenu menuRoomChat;
		private boolean f = true;

	    
		public PopupInMyListFriendListener(JPopupMenu popup) {
			this.popup = popup;
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			
			
		}

		public void mousePressed(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    public void mouseReleased(MouseEvent e) {
	        maybeShowPopup(e);
	    }

	    private boolean maybeShowPopup(MouseEvent e) {
	        if (e.isPopupTrigger()) {
	        	JButton but = (JButton) e.getComponent();
	        	if(f )
	        	{
	        		
		        	JMenuItem menuItem = new JMenuItem("Create Room Chat");
		        	menuItem.addActionListener(new ActionListener(){
	
						@Override
						public void actionPerformed(ActionEvent arg0) {
							// TODO Auto-generated method stub
							
							System.out.println("Create Room Chat " + but.getText());
							clientController.createRoomChat(but.getText());
							
						}
		        		
		        	});
		        	menuRoomChat = new JMenu("Add Friend To Room");
		        	
		        	popup.add(menuItem);
		        	popup.add(menuRoomChat);
		        	f = false;
	        	}
	        	menuRoomChat.removeAll();
	        	menuRoomChat.repaint();
	        	
	        	ArrayList<String> roomchat = clientController.getListRoomChatExcept(but.getText());
	        	for(String nameroom: roomchat)
	        	{
	        		JMenuItem menuItem = new JMenuItem(nameroom);
	        		menuRoomChat.add(menuItem);
	        	}
	        	menuRoomChat.revalidate();
	        	menuRoomChat.repaint();
	        	
	            popup.show(e.getComponent(),
	                       e.getX(), e.getY());
	            return true;
	        }
	        return false;
	    }
	}
	
	@Override
	public void updateListFriendInRoom(ArrayList<MyFriend> listFriends) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater( 
	            new Runnable() {
	                public void run() {
	                	panel_list_friend_in_room.removeAll();
	                	panel_list_friend_in_room.repaint();
	                	for(int i = 0; i < listFriends.size(); i++)
	            		{
	                	
	                		JButton buttonx = new JButton(listFriends.get(i).getmUserName());
	            			buttonx.setAlignmentX(Component.CENTER_ALIGNMENT);
	            			buttonx.setFont(new Font("Tahoma", Font.PLAIN, 19));
	            			if(listFriends.get(i).ismStatus())
	            			{
	            				buttonx.setForeground(new Color(0, 128, 0));
	            			}
	            			else{
	            				buttonx.setForeground(Color.RED);
	            			}
	            			panel_list_friend_in_room.add(buttonx);
	            			
	            		}
	                	panel_list_friend_in_room.revalidate();
	                	panel_list_friend_in_room.repaint();
	                }
	            }
	        );
	}


	@Override
	public void notifyMessageOnScreen(String roomName, JPanel panel_textmessage2) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater( 
	            new Runnable() {
	                public void run() {
	                	if(lb_chatvsFriends.getText().equals(roomName))
	                	{
	                		panel_textmessage = panel_textmessage2;
	                		scrollPane.setViewportView(panel_textmessage);
	                		//scrollPane
	                	}
	                }
	            }
	        );
	}


	@Override
	public void updateGUIListAvailableFriends(HashMap<String, MyFriend> listAvaiFriends, HashMap<String, MyFriend> listMyFriends) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater( 
	            new Runnable() {
	                public void run() {
	                	panel_list_available_friends.removeAll();
	                	panel_list_available_friends.repaint();
	                	for(Entry<String, MyFriend> entry : listAvaiFriends.entrySet()) {
	                	    MyFriend value = entry.getValue();
	                	    
	                	    if(!IsContainFriend(value.getmUserName(), listMyFriends))
	                	    {
		                	    JPopupMenu popup;
	
		                	    //...where the GUI is constructed:
		                	    //Create the popup menu.
		                	    popup = new JPopupMenu();
		                	    
		                	    
		                	    
		                	   
	
		                	    //Add listener to components that can bring up popup menus.
		                	    MouseListener popupListener = new PopupListener(popup);
		                	    
		                	    JButton buttonx = new JButton(value.getmUserName());
		            			buttonx.setAlignmentX(Component.CENTER_ALIGNMENT);
		            			buttonx.setFont(new Font("Tahoma", Font.PLAIN, 19));
		            			if(value.ismStatus())
		            			{
		            				buttonx.setForeground(new Color(0, 128, 0));
		            			}
		            			else{
		            				buttonx.setForeground(Color.RED);
	
		            			}
		            			buttonx.addMouseListener(popupListener);
		            			
		            			listButAvailableFriends.put(value.getmUserName(), buttonx);
		            			panel_list_available_friends.add(buttonx);
	                	    }
	            			
	            		}
	                	
	                	panel_list_available_friends.revalidate();
	                	panel_list_available_friends.repaint();
                	
	                }

					private boolean IsContainFriend(String getmUserName, HashMap<String, MyFriend> listMyFriends) {
						// TODO Auto-generated method stub
						return listMyFriends.containsKey(getmUserName) ? true:false;
					}
	            }
	        );
	}


	@Override
	public void updateGUIHaveFriendOnline(String mUserName) {
		// TODO Auto-generated method stub
		SwingUtilities.invokeLater( 
	            new Runnable() {
	                public void run() {
	                	JButton butOffline = listButFriendsOffline.get(mUserName);
	                	panel_list_offline.remove(butOffline);
	                	
	                	JButton buttonOnline = new JButton(mUserName);
        				buttonOnline.setAlignmentX(Component.CENTER_ALIGNMENT);
        				buttonOnline.setFont(new Font("Tahoma", Font.PLAIN, 19));
        				buttonOnline.setForeground(new Color(0, 128, 0));
        				
        				panel_list_online.add(buttonOnline);
        				
        				JButton but_in_list_friend = listButFriends.get(mUserName);
        				but_in_list_friend.setForeground(new Color(0, 128, 0));
        				//panel_list_friend
        				JButton but_in_list_available_friend = listButAvailableFriends.get(mUserName);
        				but_in_list_available_friend.setForeground(new Color(0, 128, 0));
        				
        				panel_list_offline.revalidate();
        				panel_list_offline.repaint();
        				panel_list_online.revalidate();
        				panel_list_online.repaint();

        				panel_list_friend.revalidate();
        				panel_list_friend.repaint();
        				panel_list_available_friends.revalidate();
	                	panel_list_available_friends.repaint();
        				
	                }
	            }
	        );
	}


	@Override
	public void createAccountSuccess() {
		// TODO Auto-generated method stub
		this.label_status_createac.setText("Create Account Success !!!");
		SwingUtilities.invokeLater( 
	            new Runnable() {
	                public void run() {
	                	for(int i = 0; i < 1; i++)
	                	{
	                		try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
	                	}
	                	MainClient.this.setVisible(false);
	                	MainClient.this.setBounds(100, 100, 1024, 720);
	                	MainClient.this.setVisible(true);
	                	panel_login.setVisible(false);
	                	panel_chat.setVisible(true);
	                	lb_userName.setText("Username: " + clientController.getmMember().getUserName());
	                	if(clientController.getmMember().isStatus())
	                		lb_Status.setText("Status: " +"Online");
	                	
	                }
	            }
	        );
	}


	@Override
	public void createAccountFailed() {
		// TODO Auto-generated method stub
		this.label_status_createac.setText("Create Account Failed !!!");
	}


	@Override
	public void changeRoomNameFailed() {
		// TODO Auto-generated method stub
		this.showAlertDialog("Không thể thay đổi tên phòng");
	}


	@Override
	public void changeRoomNameSuccess(String oldName, String newName) {
		// TODO Auto-generated method stub
		//this.showAlertDialog("Thay đổi tên phòng thành công: " + oldName + "...." + newName);
		JButton button = listButRooms.get(oldName);
		listButRooms.remove(oldName);
		
		button.setText(newName);
		if(lb_chatvsFriends.getText().equals(oldName))
			lb_chatvsFriends.setText(newName);
		
		listButRooms.put(newName, button);
	}


	@Override
	public void okExitWindows() {
		// TODO Auto-generated method stub
		System.exit(0);
	}
}
