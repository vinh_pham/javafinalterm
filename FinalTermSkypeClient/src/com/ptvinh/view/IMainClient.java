package com.ptvinh.view;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

import com.ptvinh.model.MyFriend;
import com.ptvinh.model.MyMessage;

public interface IMainClient {

	void loginSuccess();

	void loginFailed();

	void updateIncomingMessage(String message);

	void updateGUIListFriend(HashMap<String,MyFriend> ListFriends);

	void updateListRooms(ArrayList<String> roomNames);

	void updateListFriendInRoom(ArrayList<MyFriend> listFriends);


	void notifyMessageOnScreen(String roomName, JPanel panel_textmessage);

	void updateGUIListAvailableFriends(HashMap<String, MyFriend> listAvaiFriends, HashMap<String, MyFriend> listMyFriends);

	void updateGUIHaveFriendOnline(String mUserName);

	void createAccountSuccess();

	void createAccountFailed();

	void changeRoomNameFailed();

	void changeRoomNameSuccess(String oldName, String newName);

	void okExitWindows();

}
