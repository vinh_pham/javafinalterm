package com.ptvinh.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.ptvinh.controller.ServerController;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JLabel;
import java.awt.Font;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.JTextArea;
import javax.swing.JList;
import javax.swing.AbstractListModel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainServer extends JFrame implements IServerView{

	private JPanel contentPane;
	private static JTextArea txt_Message;
	private static ServerController controller;
	private static JLabel lb_infor;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			

			public void run() {
				try {
					MainServer frame = new MainServer();
					frame.setVisible(true);
										
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		
	}
	
	static ServerSocket listener;
	
	
	
	

	/**
	 * Create the frame.
	 */
	public MainServer() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 978, 567);
		setTitle("Server");
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnTools = new JMenu("Tools");
		menuBar.add(mnTools);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("New menu item");
		mnTools.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("New menu item");
		mnTools.add(mntmNewMenuItem_1);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JLabel lblServerChat = new JLabel("Server Chat");
		lblServerChat.setHorizontalAlignment(SwingConstants.CENTER);
		lblServerChat.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 17));
		contentPane.add(lblServerChat, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.WEST);
		panel.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);
		
		
		
		JLabel lblNewLabel = new JLabel("Users are online");
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setBackground(Color.ORANGE);
		lblNewLabel.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));
		panel.add(lblNewLabel, BorderLayout.NORTH);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panel_1.add(scrollPane_1, BorderLayout.CENTER);
		scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		
		JTextArea txt_Communication = new JTextArea();
		txt_Communication.setEditable(false);
		scrollPane_1.setViewportView(txt_Communication);
		
		this.txt_Message = txt_Communication;
		
		JLabel lblCommunication = new JLabel("Communication");
		lblCommunication.setHorizontalAlignment(SwingConstants.CENTER);
		lblCommunication.setForeground(Color.RED);
		lblCommunication.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));
		lblCommunication.setBackground(Color.ORANGE);
		panel_1.add(lblCommunication, BorderLayout.NORTH);
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2, BorderLayout.SOUTH);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JLabel lblIpNull_1 = new JLabel("Server is running");
		lblIpNull_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblIpNull_1.setForeground(Color.RED);
		lblIpNull_1.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));
		lblIpNull_1.setBackground(Color.ORANGE);
		panel_2.add(lblIpNull_1, BorderLayout.CENTER);
		
		lb_infor = lblIpNull_1;
		
		JPanel panel_3 = new JPanel();
		panel_2.add(panel_3, BorderLayout.WEST);
		panel_3.setLayout(new BorderLayout(0, 0));
		
		JLabel lblIpNull = new JLabel("IP: null       ");
		lblIpNull.setHorizontalAlignment(SwingConstants.RIGHT);
		lblIpNull.setForeground(Color.RED);
		lblIpNull.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));
		lblIpNull.setBackground(Color.ORANGE);
		panel_3.add(lblIpNull, BorderLayout.CENTER);
		
		JLabel lb_port = new JLabel("Port: null      ");
		panel_3.add(lb_port, BorderLayout.WEST);
		lb_port.setHorizontalAlignment(SwingConstants.RIGHT);
		lb_port.setForeground(Color.RED);
		lb_port.setFont(new Font("Segoe UI Semilight", Font.PLAIN, 14));
		lb_port.setBackground(Color.ORANGE);
		
		JButton but_exitserver = new JButton("RUN SERVER");
		but_exitserver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				new Thread(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						controller = new ServerController(MainServer.this);
						controller.GetDataMember();
						try {
							listener = new ServerSocket(9999);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						while(true)
						{
							
							try {

								System.out.println("Waiting for new Client");
								Socket socket = listener.accept();
								controller.addConnector(socket);
								notifyGUIAddingSocket(socket.getPort());
								notifyMessage("Add new Client: " + socket.getPort());
								
								
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
					
				}.start();
				
			}
		});
		panel_2.add(but_exitserver, BorderLayout.EAST);
		
		
	}

	
	private static void notifyGUIAddingSocket(int port) {
		// TODO Auto-generated method stub
		
		SwingUtilities.invokeLater( 
	            new Runnable() {
	                public void run() {
	                	lb_infor.setText("Display: Add new Client " + port);
	                	System.out.println("Display: Add new Client " + port);
	                	try {Thread.sleep(3000);
	                		lb_infor.setText("Server is Running");
	                		} catch(InterruptedException e) {};
	                }
	            }
	        );
		
	}


	@Override
	public void updateGUI(String line) {
		// TODO Auto-generated method stub
		notifyMessage(line);
		
	}
	private static void notifyMessage(String line) {
		// TODO Auto-generated method stub
		
		SwingUtilities.invokeLater( 
	            new Runnable() {
	                public void run() {
	                	txt_Message.append(line + "\r\n");
	                }
	            }
	        );
		
	}
}
