package com.ptvinh.model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DataAccess {
	HashMap<String, MyMember> listMyMember = new HashMap<String, MyMember>();
	
	List<MyRoom> listRoom = new ArrayList<MyRoom>();
	
	private void GetAllUserFromDatabase() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		try {
			File inputFile = new File("User.xml");
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println(doc.getDocumentElement().getNodeName());
		
			NodeList nListUser = doc.getElementsByTagName("user");
			for (int i = 0; i < nListUser.getLength(); i++) {
				Node nNodeUser = nListUser.item(i);
				Element eElement = (Element) nNodeUser;
				String userName = eElement.getAttribute("username");
				String passWord = eElement.getElementsByTagName("password").item(0).getTextContent();

				//add vao listMyMember
				MyMember myMember = new MyMember(userName, passWord, false);
				listMyMember.put(userName, myMember);
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void GetAllFriendListFromDatabase() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		try {
			File inputFile = new File("FriendList.xml");
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println(doc.getDocumentElement().getNodeName());
		
			NodeList nListUser = doc.getElementsByTagName("user");
			for (int i = 0; i < nListUser.getLength(); i++) {
				Node nNodeUser = nListUser.item(i);
				Element eElement = (Element) nNodeUser;
				
				// moi user co 1 hashmap listfriend
				HashMap<String, MyFriend> listMyFriend = new HashMap<String, MyFriend>();
				String userName = eElement.getAttribute("username");

				// lay tat ca cac node co tag la "friend"
				NodeList nListFriend = eElement.getElementsByTagName("friend");
				// voi moi tag la 1 node => get content cua node do
				for (int j = 0; j < nListFriend.getLength(); j++) {
					Node nNodeFriend = nListFriend.item(j);
					String friendName = nNodeFriend.getTextContent();
					
					// voi moi friend cua user, add thanh 1 list friend
					listMyFriend.put(friendName, new MyFriend(friendName, false));
				}
				
				listMyMember.get(userName).setListFriend(listMyFriend);
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void GetAllChatRoomFromDatabase() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		try {
			File inputFile = new File("ChatRoom.xml");
			
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(inputFile);
			doc.getDocumentElement().normalize();
			//System.out.println(doc.getDocumentElement().getNodeName());
		
			// lay danh sach cac tag "room"
			NodeList nListRoom = doc.getElementsByTagName("room");
			// voi moi room co list user va list message
			for (int i = 0; i < nListRoom.getLength(); i++) {
				Node nNodeRoom = nListRoom.item(i);
				Element eElementRoom = (Element) nNodeRoom;
				
				// lay room name
				String roomName = eElementRoom.getAttribute("roomname");
				
				// lay danh sach cac user trong room do
				HashMap<String, MyFriend> listUsersInRoom = new HashMap<String, MyFriend>();
				NodeList nNodeListUser = eElementRoom.getElementsByTagName("listuser");
				for (int j = 0; j < nNodeListUser.getLength(); j++) {
					Element user = (Element) nNodeListUser.item(j);
					
					// lay danh sach cac tag "user"
					NodeList nListUser = user.getElementsByTagName("user");
					// voi moi tag user add vao listUserInRoom
					for (int k = 0; k < nListUser.getLength(); k++) {
						Node nNodeUser = nListUser.item(k);
						String username = nNodeUser.getTextContent();
						System.out.println(username);
						// add vao listUserInroom
						listUsersInRoom.put(username, new MyFriend(username, false));
					}
				}
				
				// lay danh sach cac message trong room
				ArrayList<MyMessage> listMessagesInRoom = new ArrayList<MyMessage>();	
				NodeList nNodeListMessages = eElementRoom.getElementsByTagName("listmessages");
				for (int j = 0; j < nNodeListMessages.getLength(); j++) {
					Element messages = (Element) nNodeListMessages.item(j);
					NodeList nListMessage = messages.getElementsByTagName("message");
					
					for (int k = 0; k < nListMessage.getLength(); k++) {
						Node nodeMessage = nListMessage.item(k);
						Element eElementMessage = (Element) nodeMessage;
						String sender = eElementMessage.getElementsByTagName("sender").item(0).getTextContent();
						String content = eElementMessage.getElementsByTagName("content").item(0).getTextContent();
						System.out.println(sender + ": " + content);
						listMessagesInRoom.add(new MyMessage(sender, content));
					}
				}
				
				// Tao 1 Room gom cac thong tin RoomName, list User in room, list Message in room
				MyRoom room = new MyRoom(roomName, listUsersInRoom, listMessagesInRoom);
				
				// voi moi listUsersInRoom, xem tat ca cac user trong database, ai co trong list nay thi add vao
				for (Entry<String, MyFriend> entry : listUsersInRoom.entrySet()) {
					String key = entry.getKey();
					// neu member nam trong room nay thi member do add room nay
					if (listMyMember.containsKey(key)) {
						listMyMember.get(key).addRoom(room);
					}
				}			
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public HashMap<String, MyMember> GetDataMembersFromDatabase() {
		// TODO Auto-generated method stub
		GetAllUserFromDatabase();
		GetAllFriendListFromDatabase();
		GetAllChatRoomFromDatabase();
		
		return listMyMember;
	}

	////////////////////////////////////////////////////////////////////////////////
	//write
	
	public void AddMessageHistory(String roomName, String sender, String content) {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		try {
			File file = new File("ChatRoom.xml");
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(file);
			
			// get node co tag la <room>
			NodeList nodeList = doc.getElementsByTagName("room");
			
			// tim tag nao co attribute dung voi roomname truyen vao
			for (int i = 0; i < nodeList.getLength(); i++) {
				String attr = nodeList.item(i).getAttributes().getNamedItem("roomname").getNodeValue();
				// neu duyet qua node nay, va ten attribute = vs roomName
				if (attr.equals(roomName)) {
					Element roomElement = (Element) nodeList.item(i);
					NodeList nodeListMessages = roomElement.getElementsByTagName("listmessages");
					
					for (int j = 0; j < nodeListMessages.getLength(); j++) {
						Element listMessage = (Element) nodeListMessages.item(j);
						
						// create element <message> in <listmessages>
						Element message = doc.createElement("message");
						// add vao element listMessage
						listMessage.appendChild(message);
						
						// trong message co sender va content
						// tao 2 element <sender> va <content> in <message>
						Element senderElement = doc.createElement("sender");
						senderElement.appendChild(doc.createTextNode(sender));
						
						Element contentElement = doc.createElement("content");
						contentElement.appendChild(doc.createTextNode(content));
						
						// add vao <message>
						message.appendChild(senderElement);
						message.appendChild(contentElement);
						
						// ghi vao xml
						TransformerFactory transformerFactory = TransformerFactory.newInstance();
						Transformer transformer = transformerFactory.newTransformer();
						
						DOMSource source = new DOMSource(doc);
						StreamResult result = new StreamResult(file);
						transformer.transform(source, result);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	public void AddFriend(String yourName, String friendName) {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		try {
			File file = new File("FriendList.xml");
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(file);
			
			// get node co tag la <user>
			NodeList nodeList = doc.getElementsByTagName("user");
			
			// tim tag nao co attribute dung voi name truyen vao
			for (int i = 0; i < nodeList.getLength(); i++) {
				String attr = nodeList.item(i).getAttributes().getNamedItem("username").getNodeValue();
				// neu duyet qua node nay, va ten attribute = vs yourName
				if (attr.equals(yourName)) {
					// create element <friend>
					Element yourElement = (Element) nodeList.item(i);
					Element friendElement = doc.createElement("friend");
					friendElement.appendChild(doc.createTextNode(friendName));
					yourElement.appendChild(friendElement);
					
					// ghi vao xml
					TransformerFactory transformerFactory = TransformerFactory.newInstance();
					Transformer transformer = transformerFactory.newTransformer();
					
					DOMSource source = new DOMSource(doc);
					StreamResult result = new StreamResult(file);
					transformer.transform(source, result);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	public void UserRegister(MyMember member) {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		try {
			File file = new File("User.xml");
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(file);
			
			// lay root
			Element rootElement = doc.getDocumentElement();
			
			// tao 1 tag user
			Element userElement = doc.createElement("user");
			// root add tag user vao
			rootElement.appendChild(userElement);
			
			// set attribute cho tag user
			Attr attr = doc.createAttribute("username");
			attr.setValue(member.getUserName());
			userElement.setAttributeNode(attr);
			
			// set tag password
			Element passElement = doc.createElement("password");
			passElement.appendChild(doc.createTextNode(member.getPassword()));
			userElement.appendChild(passElement);
		
			
			// ghi vao xml
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(file);
			transformer.transform(source, result);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
}
