package com.ptvinh.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import com.ptvinh.controller.IController;

public class MyMember implements IMyMember{

	String userName;
	String password;
	boolean status;
	
	Connector conn;
	HashMap<String, MyFriend> listFriend;
	HashMap<String, MyRoom> listRoom;
	private IController iController;
	private static String FRoomName;
	private static String FfileName;
	private static String FsizeDatabyte;
	
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
		this.setTrueorFalseInAllFriends(status);
		if(status)
		{
			this.notifyImOnline(); // báo hiệu cho các bạn tôi rằng tôi online
		}else
		{
			this.notifyImOffline(); // báo hiệu cho các bạn tôi rằng tôi offline
			this.conn.okExitWindow();
		}
	}
	
	
	
	private void notifyImOffline() {
		// TODO Auto-generated method stub
		this.iController.sendMessageImOnlineorOffline(false,this.userName);
		
	}
	public HashMap<String, MyFriend> getListFriend() {
		return listFriend;
	}
	public void setListFriend(HashMap<String, MyFriend> listFriend) {
		this.listFriend = listFriend;
	}
	public MyMember(String userName, String password, boolean status) {
		// TODO Auto-generated constructor stub
		
		this.userName = userName;
		this.password = password;
		this.status = status;
		this.listFriend = new HashMap<String, MyFriend> ();
		this.listRoom = new HashMap<String, MyRoom>();
		
		
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Connector getConn() {
		return conn;
	}
	public void setConn(Connector conn) {
		this.conn = conn;
		this.conn.setIMyMember(this);
	}
	public void addFriend(MyMember user) {
		// TODO Auto-generated method stub
		this.listFriend.put(user.userName, new MyFriend(user.userName, user.status));
		
	}
	public void sendListFriend() {
		// TODO Auto-generated method stub
		// gửi list friend qua cho client
		String datalistfriend = ToStringListFriend(this.listFriend);
		
		conn.sendListFriend(datalistfriend);
		
	}
	private String ToStringListFriend(HashMap<String, MyFriend> listFriend) {
		// TODO Auto-generated method stub
		
		String result = "";
		for(Entry<String, MyFriend> entry : listFriend.entrySet()) {
		    MyFriend value = entry.getValue();
		    result += value.getmUserName() +"-" + value.ismStatus() + "/";
		}
		
		
		return result;
	}
	public void sendAnyData() {
		// TODO Auto-generated method stub
		String datalistfriend = ToStringListFriend(this.listFriend);
		this.conn.sendAnyData(datalistfriend);
		
	}
	public void addRoom(MyRoom room1) {
		// TODO Auto-generated method stub
		listRoom.put(room1.getRoomName(), room1);

	}
	
	public void listenDataFromClient() {
		// TODO Auto-generated method stub
		this.conn.listenAnyDataFromClient();
	}
	@Override
	public void recieveRequestFromClient(byte[] b) {
		// TODO Auto-generated method stub
		switch(checkRequest(b))
		{
			case 1: // Send ALL ROOMS
				this.iController.sendMessagetoController(this.userName + " request " + new String(b));
				
				sendListRoom();
				
				
				break;
			case 2: // Client Sent Message
				this.iController.sendMessagetoController(this.userName + " send Message " + new String(b));
				
				sendMessageToRoom(b);
				
				break;
			case 3: // Get All List Available Members
				this.iController.sendMessagetoController(this.userName + " request " + new String(b));
				
				this.iController.getAllListMembers(this.userName);
				
				break;
			case 4: // Add New Friend
				this.iController.sendMessagetoController(this.userName + " request " + new String(b));
				
				String userNameNewFriend = getuserNameNewFriend(b);
				if(!IsExistFriend(userNameNewFriend))
				{
					this.iController.addNewFriend(this.userName, userNameNewFriend);
				}
				
				break;
			case 5: // Create Room Chat with Friend
				this.iController.sendMessagetoController(this.userName + " request " + new String(b));
				
				String userFriend = getuserNameNewFriend(b);
				String RoomName = this.userName + "vs" + userFriend;
				if(!IsExistRoomChat(RoomName) && !IsExistRoomChat(userFriend  + "vs" + this.userName))
				{
					this.iController.sendMessagetoController("Create Room Chat: " + RoomName);
			
					MyRoom room = new MyRoom(RoomName);
					room.addFriend(new MyFriend(this.userName, this.isStatus()));
					addRoom(room);
					
					
					this.iController.addRoomToMember(this.userName, userFriend, room);
				}
				
				break;
			case 6: // Change ROOM NEW NAME
				this.iController.sendMessagetoController(this.userName + " request " + new String(b));
				
				String OldName = getRoomOldName(b);
				String NewName = getRoomNewName(b);
				MyRoom room = this.listRoom.get(OldName);
				
				this.iController.checkRoomNewName(this.userName, room, NewName);
				
				break;
			case 7: // GET ALL HISTORY MESSAGE IN ROOM
				this.iController.sendMessagetoController(this.userName + " request " + new String(b));
				
				String RoomNameHis = getRoomName(b);
				
				if(IsExistRoomChat(RoomNameHis))
				{
					MyRoom romHis = this.listRoom.get(RoomNameHis);
					this.conn.sendListHistoryMessageToClient(RoomNameHis, romHis.ToStringListMessage());
				}
				
				break;
			case 8: // Exit Window
				this.iController.sendMessagetoController(this.userName + " request " + new String(b));
				
				this.setStatus(false);
				
				break;
			
		}
	}
	private static void testting() {
		File file = new File("D:\\icon.jpg");
		String code ="ABC" +"RoomName" +"/" + "FfileName" + "/";
		try {
			byte[] data = new byte[(int) file .length()];
			FileInputStream fis;
			fis = new FileInputStream(file);
			int count = fis.read(data);
			fis.close();
			byte[] DATA = coupleArrayByte((code+count+"/").getBytes(), data, count);
			
			
			getdataFile(DATA);
			
			int offset = 3 + FfileName.getBytes().length + FRoomName.getBytes().length + FsizeDatabyte.getBytes().length +3;
			byte[] dataXXX = TrimArrayByte(DATA, offset, Integer.parseInt(FsizeDatabyte));
			
			 File filex = new File("D:\\icon9999.jpg");
			 FileOutputStream fos = new FileOutputStream(filex);
			 fos.write(dataXXX);
			 fos.close();
			 
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	private static byte[] coupleArrayByte(byte[] code, byte[] data, int count) {
		// TODO Auto-generated method stub
		byte[] re = new byte[code.length + count];
		int j = 0;
		for(int i = 0; i < code.length + count; i++)
		{
			
			if(i < code.length)
				re[i] = code[i];
			else
			{
				re[i] = data[j];
				j++;
			}
		}
		return re;
	}

	private static byte[] TrimArrayByte(byte[] b, int offset, int sizeDatabyte) {
		// TODO Auto-generated method stub
		byte[] res = new byte[sizeDatabyte];
		for(int i = 0; i < sizeDatabyte; i++)
		{
			res[i] = b[offset++];
		}
		return res;
	}
	private static void getdataFile(byte[] b) {
		// TODO Auto-generated method stub
		String byteToStr = new String(b);
		 
	  	 byteToStr = byteToStr.substring(3);
	  	
	  	 String[] result = byteToStr.split("/");
	  	FRoomName = result[0];
	  	FfileName = result[1];
	  	FsizeDatabyte  =result[2]; 
	  
	  	
	}
	
		
	private String getRoomNewName(byte[] b) {
		// TODO Auto-generated method stub
		String byteToStr = new String(b);
		 
		 // bỏ 3 ký tự "SMS"
	  	 byteToStr = byteToStr.substring(3);
	  	
	  	 String[] result = byteToStr.split("/");
	  	 
		 return result[1];
	}
	private String getRoomOldName(byte[] b) {
		// TODO Auto-generated method stub
		String byteToStr = new String(b);
		 
		 // bỏ 3 ký tự "SMS"
	  	 byteToStr = byteToStr.substring(3);
	  	
	  	 String[] result = byteToStr.split("/");
	  	 
		 return result[0];
	}
	public void sendListRoom() {
		if(this.isStatus())
		{
			if(this.listRoom.size() != 0)
			{
				String dataRoom = "LAR";
				
				for(Entry<String, MyRoom> entry : this.listRoom.entrySet()) {
				    MyRoom value = entry.getValue();
				    dataRoom += "/" + value.toString();
				}
				this.iController.sendMessagetoController(this.userName + ": ALL ROOMS: " + dataRoom);
				

				this.conn.sendListRooms(dataRoom.getBytes());
			}
			else{
				this.iController.sendMessagetoController(this.userName + ": ALL ROOMS: " + "nothing");
			}
		}
	}
	private boolean IsExistRoomChat(String RoomName) {
		// TODO Auto-generated method stub
		return this.listRoom.containsKey(RoomName) ? true:false;
	}
	private String getuserNameNewFriend(byte[] b) {
		// TODO Auto-generated method stub
		String byteToStr = new String(b);
		 
		 // bỏ 3 ký tự "SMS"
	  	 byteToStr = byteToStr.substring(3);
	  	
	  	 String[] result = byteToStr.split("/");
	  	 
		 return result[0];
	}
	private void sendMessageToRoom(byte[] b) {
		// TODO Auto-generated method stub
		
		String RoomName = getRoomName(b);
		String Message = getMessage(b);
		
		if(IsExitsRoom(RoomName))
		{
			MyRoom room = this.listRoom.get(RoomName);
			room.sendMessageAllFriend(iController,new MyMessage(this.userName,Message));
		}
		
		
	}
	private boolean IsExitsRoom(String roomName) {
		// TODO Auto-generated method stub
		return this.listRoom.containsKey(roomName) ? true:false;
	}
	
	public String getRoomName(byte[] data) {
		 String byteToStr = new String(data);
		
		 // bỏ 3 ký tự "SMS"
	  	 byteToStr = byteToStr.substring(3);
	  	
	  	 String[] result = byteToStr.split("/");
	  	 
		 return result[0];
	 }
	 
	 public String getMessage(byte[] data) {
		 String byteToStr = new String(data);
		 
		 // bỏ 3 ký tự "SMS"
	  	 byteToStr = byteToStr.substring(3);
	  	
	  	 String[] result = byteToStr.split("/");
	  	 
		 return result[1];
	 }
	
	private int checkRequest(byte[] b) {
		// TODO Auto-generated method stub
		byte[] tem = new byte[3];
		tem[0] = b[0];
		tem[1] = b[1];
		tem[2] = b[2];
		String res = new String(tem);
		
		if(res.equals(Connector.KEY_ALLROOM))
		{
			return 1;
		}
		if(res.equals(Connector.KEY_SENMESSAGE))
		{
			return 2;
		}
		if(res.equals(Connector.KEY_LISTAVAILABLEFRIEND))
		{
			return 3;
		}
		if(res.equals(Connector.KEY_ADDNEWFRIEND))
		{
			return 4;
		}
		if(res.equals(Connector.KEY_CREATEROOMCHAT))
		{
			return 5;
		}
		if(res.equals(Connector.KEY_ROOMNEWNAME))
		{
			return 6;
		}
		if(res.equals(Connector.KEY_HISTORYMESSAGEINROOM))
		{
			return 7;
		}
		if(res.equals(Connector.KEY_EXITWDW))
		{
			return 8;
		}
		
		return 0;
	}
	public void setIController(IController iserverController) {
		// TODO Auto-generated method stub
		this.iController = iserverController;
	}
	public void sendMessageToClient(String roomName, MyMessage myMessage) {
		// TODO Auto-generated method stub
		if(this.isStatus())
		{
			this.conn.sendMessageToClient(roomName, myMessage);
		}
		
	}
	public void sendListAvaiableFriends(String dataMember) {
		// TODO Auto-generated method stub
		this.conn.sendListAvailableMember(dataMember);
		
	}
	public void notifyImOnline() {
		// TODO Auto-generated method stub
		// báo tín hiệu đến các bạn tôi đã online
		
		this.iController.sendMessageImOnlineorOffline(true,this.userName);
		
	}
	
	public void setTrueorFalseInAllFriends(boolean b) {
		// TODO Auto-generated method stub
		for(Entry<String, MyFriend> entry: this.listFriend.entrySet())
		{
			MyFriend fr = entry.getValue();
			this.iController.setTrueorFalseInAllFriends(fr.mUserName,this.userName ,b);
		}
		
	}
	public void setTrueorFalseForFriend(String userNamefr, boolean b) {
		// TODO Auto-generated method stub
		if(IsExistFriend(userNamefr))
		{
			MyFriend fr = this.listFriend.get(userNamefr);
			fr.setmStatus(b);
		}
		
	}
	private boolean IsExistFriend(String userNamefr) {
		// TODO Auto-generated method stub
		return this.listFriend.containsKey(userNamefr);
	}
	public void sendToClientAddingNewFriendSuccess(String userNameFriend) {
		// TODO Auto-generated method stub
		if(this.isStatus())
			this.conn.sendMessageAddingNewFriendSuccess(userNameFriend);
		
	}
	public boolean checkRoomNewName(String newName) {
		// TODO Auto-generated method stub
		return this.listRoom.containsKey(newName) ? true:false;
	}
	public void notifyRoomNewNameFailed() {
		// TODO Auto-generated method stub
		this.conn.notifyRoomNewNameFailed();
	}
	public void changeRoomNewName(String oldName, String newName) {
		// TODO Auto-generated method stub
		if(IsExistRoomChat(oldName))
		{
			MyRoom room = this.listRoom.get(oldName);
			room.setRoomName(newName);
			this.listRoom.remove(room);
			this.listRoom.put(newName, room);
		}
	}
	public void notifyRoomNewNameSuccess(String oldName, String newName) {
		// TODO Auto-generated method stub
		this.conn.notifyRoomNewNameSuccess(oldName,newName);
	}
	public void sendToClientHaveFriendOffline(String muserName) {
		// TODO Auto-generated method stub

		this.conn.sendToClientHaveFriendOffline(muserName);
	}
	public void sendToClientHaveFriendOnline(String muserName) {
		// TODO Auto-generated method stub
		if(this.status)
			this.conn.sendToClientHaveFriendOnline(muserName);
		
	}
	@Override
	public void writeToFile(String roomName, String fileName, Integer filsize, byte[] dataXXX) {
		// TODO Auto-generated method stub
		this.iController.sendMessagetoController(this.userName + " send File: " + fileName + "to Room: " + roomName);
		
		String path = getPathFileType(roomName,fileName);
		File filex = new File(path);
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(filex);
			fos.write(dataXXX);
			fos.close();
			this.sendFileToRoom(roomName, fileName, filsize, dataXXX);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void sendFileToRoom(String roomName, String fileName, Integer filsize, byte[] dataXXX) {
		// TODO Auto-generated method stub

		if(IsExitsRoom(roomName))
		{
			MyRoom room = this.listRoom.get(roomName);
			MyMessage message = new MyMessage(this.userName, "Send File: " + fileName);
			room.sendFileAllFriend(iController,message,roomName,fileName,filsize,dataXXX);
		}
	}
	private String getPathFileType(String roomName, String fileName) {
		// TODO Auto-generated method stub
		
		return "DataItems"+"\\"+fileName;
	}
	public void sendFileToClient(String roomName2, MyMessage message, String fileName, Integer filsize,
			byte[] dataXXX) {
		// TODO Auto-generated method stub
		this.conn.sendFileToClient(roomName2,message,fileName,filsize,dataXXX);
	}
	
	
	

}
