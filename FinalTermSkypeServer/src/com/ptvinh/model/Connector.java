package com.ptvinh.model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

import com.ptvinh.controller.IController;
import com.ptvinh.controller.ServerController;

public class Connector {
	private boolean running = true;


	public static final String KEY_ALLLISTFRIEND = "LFS";
	public static final String KEY_ALLROOM = "LAR";
	public static final String KEY_SENMESSAGE = "SMS";
	public static final String KEY_LISTAVAILABLEFRIEND = "AFS";
	public static final String KEY_HAVEFRIENDONLINE = "HFO";
	public static final String KEY_ADDNEWFRIEND = "ANF";
	public static final String KEY_ADDNEWFRIENDSUCCESS = "SNF";
	public static final String KEY_ADDNEWFRIENDFAILED = "FNF";
	public static final String KEY_CREATEROOMCHAT = "CRC";
	public static final String KEY_ROOMNEWNAME = "RNN";
	public static final String KEY_RoomNewNameFailed = "RNF";
	public static final String KEY_RoomNewNameSuccess = "RNS";
	public static final String KEY_HISTORYMESSAGEINROOM = "HAR";
	public static final String KEY_EXITWDW = "EXI";
	public static final String KEY_SENDFILE = "SEF";


	public static final String KEY_HAVEFRIENDOFFLINE = "HFF";
	public static final String CodeCut = "CodeCutPP";

	public static final int FileSize = 7 * 1000000;





	private DataOutputStream out;
	private DataInputStream in;
	public IController iController;
	private InputStream inFromServer;
	private String NameSocket;
	private IMyMember imyMember;


	private OutputStream outToServer;

	
	public String getNameSocket() {
		return NameSocket;
	}
	public void setNameSocket(String nameSocket) {
		NameSocket = nameSocket;
	}
	public Connector(Socket newSocketClient, IController iController) {
		// TODO Auto-generated constructor stub
		try {
			outToServer = newSocketClient.getOutputStream();
	        out = new DataOutputStream(outToServer);
	        
	        inFromServer = newSocketClient.getInputStream();
	        in = new DataInputStream(inFromServer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        this.iController = iController;
	}
	private String CheckFristBytes(byte a, byte b, byte c) {
		// TODO Auto-generated method stub
		// kiểm tra xem abc có mã là text hay hình ảnh. text = "TXT" hinh = "IMG"
		
		byte[] bs = new byte[3];
		bs[0] = a;
		bs[1] = b;
		bs[2] = c;
		String re = new String(bs);
		if(re.equals(Connector.KEY_EXITWDW))
		{
			return Connector.KEY_EXITWDW;
		}
		if(re.equals(Connector.KEY_SENDFILE))
		{
			return Connector.KEY_SENDFILE;
		}
		return "NULL";
	}

	public void listenVerifyAccount() {
		// TODO Auto-generated method stub
		new Thread(){

			private String line;

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
	            	byte[] b = new byte[150];
	          
	            	in.read(b);
	            	String re  = CheckFristBytes(b[0],b[1],b[2]);
	            	if(re.equals("NULL")){
	            		iController.sendAccountInfor(Connector.this, b);
	            	}

	            	
/*	            	iController.sendMessagetoController(NameClient + ": " + line2 + " : " + k);
*/	            	   
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
		}.start();;
		
	}
	private void sendMessage(byte[] bytes) {
		// TODO Auto-generated method stub
		try {
			this.out.write(bytes);	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*public void loginSucccess() {
		// TODO Auto-generated method stub
		String success = "SUC";
		sendMessage(success.getBytes());
	}*/
	
	public void loginFailed() {
		// TODO Auto-generated method stub
		String failed = "FAI";
		sendMessage(failed.getBytes());
	}
	
	
	public void sendListFriend(String datalistfriend) {
		// TODO Auto-generated method stub
		String listFriend = KEY_ALLLISTFRIEND + datalistfriend;
		sendMessage(listFriend.getBytes());
	}
	
	public void sendAnyData(String datalistfriend) {
		// TODO Auto-generated method stub
		sendListFriend(datalistfriend);
	}
	private String roomName;
	private String fileName;
	private String fileSize;
	public void listenAnyDataFromClient() {
		// TODO Auto-generated method stub
		new Thread(){

			

			@Override
			public void run() {
				// TODO Auto-generated method stub
				while(running)
				{
					try {
		            	byte[] b = new byte[FileSize];
		          
		            	in.read(b);
		            	
		            	if(CheckFristBytes(b[0],b[1],b[2]).equals(KEY_EXITWDW))
						{
							running = false;
							imyMember.recieveRequestFromClient(b);
						}
		            	else if(CheckFristBytes(b[0],b[1],b[2]).equals(KEY_SENDFILE)){
		            		fileName="";
		            		roomName="";
		            		fileSize="";
		            		
		            		parseDataFile(b);
		            		
		            		b = new byte[Integer.valueOf(fileSize)];
		            		in.readFully(b);
		            		imyMember.writeToFile(roomName,fileName,Integer.valueOf(fileSize),b);
		            	}else{
		            		imyMember.recieveRequestFromClient(b);
		            	}
		            	
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}	
		}.start();;
		
		
	}
	protected void parseDataFile(byte[] b) {
		// TODO Auto-generated method stub
		String byteToStr = new String(b);
		 
	  	 byteToStr = byteToStr.substring(3);
	  	
	  	 String[] result = byteToStr.split("/");
	  	roomName = result[0];
	  	fileName = result[1];
	  	fileSize = result[2]; 
		
	}
	public void setIMyMember(IMyMember imyMember) {
		// TODO Auto-generated method stub
		this.imyMember = imyMember;
	}
	public void sendListRooms(byte[] bytes) {
		// TODO Auto-generated method stub
		this.sendMessage(bytes);
		
	}
	public void sendMessageToClient(String roomName, MyMessage myMessage) {
		// TODO Auto-generated method stub
		String data = this.KEY_SENMESSAGE+roomName+"/"+myMessage.getmUserName()+"/"+myMessage.getmContent()+"/";
		this.sendMessage(data.getBytes());
	}
	public void sendListAvailableMember(String dataMember) {
		// TODO Auto-generated method stub
		this.sendMessage((this.KEY_LISTAVAILABLEFRIEND+dataMember).getBytes());
	}
	public void sendToClientHaveFriendOnline(String muserName) {
		// TODO Auto-generated method stub
		this.sendMessage((this.KEY_HAVEFRIENDONLINE + muserName +"/").getBytes());
		
	}
	public void createMemberFailed() {
		// TODO Auto-generated method stub
		String failed = "CAF";
		sendMessage(failed.getBytes());
	}
	public void createMemberSuccess() {
		// TODO Auto-generated method stub
		
		String suc = "CAS";
		sendMessage(suc.getBytes());
	}
	public void sendMessageAddingNewFriendSuccess(String userNameFriend) {
		// TODO Auto-generated method stub
		sendMessage((this.KEY_ADDNEWFRIENDSUCCESS+userNameFriend+"/").getBytes());
	}
	public void notifyRoomNewNameFailed() {
		// TODO Auto-generated method stub
		this.sendMessage((this.KEY_RoomNewNameFailed).getBytes());
	}
	public void notifyRoomNewNameSuccess(String oldName, String newName) {
		// TODO Auto-generated method stub
		this.sendMessage((this.KEY_RoomNewNameSuccess +oldName+"/"+ newName +"/").getBytes());
		
	}
	public void sendListHistoryMessageToClient(String roomName, String data) {
		// TODO Auto-generated method stub
		this.sendMessage((this.KEY_HISTORYMESSAGEINROOM +roomName+"/"+ data +"/").getBytes());
	}
	public void sendToClientHaveFriendOffline(String muserName) {
		// TODO Auto-generated method stub
		this.sendMessage((this.KEY_HAVEFRIENDOFFLINE + muserName +"/").getBytes());
	}
	public void okExitWindow() {
		// TODO Auto-generated method stub
		this.sendMessage((this.KEY_EXITWDW +"/").getBytes());
	}
	public void sendFileToClient(String roomName2, MyMessage message, String fileName2, Integer filesize,
			byte[] dataXXX) {
		// TODO Auto-generated method stub
		String code = this.KEY_SENDFILE + roomName2 + "/" + fileName2 + "/"+filesize+"/"+message.getmUserName() + "/"+message.getmContent()+"/";
		/*String code = this.KEY_SENDFILE;*/
		
		 
		 try {
			 if(filesize > FileSize)
			 {
				 // cannot sent file max too large
			 }
			 else
			 {
				byte[] data = new byte[filesize];
				
				this.sendMessage((code).getBytes());
				 out.write(dataXXX, 0, data.length);

			 }
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
}
