package com.ptvinh.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.ptvinh.controller.IController;

public class MyRoom {

	public HashMap<String, MyFriend> getListFriendsInRoom() {
		return listFriendsInRoom;
	}

	public void setListFriendsInRoom(HashMap<String, MyFriend> listFriendsInRoom) {
		this.listFriendsInRoom = listFriendsInRoom;
	}
	String roomName;
	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	HashMap<String, MyFriend> listFriendsInRoom;
	ArrayList<MyMessage> listMessage;
	private boolean MyFriend;
	public MyRoom(String RoomName, HashMap<String, MyFriend> listFriendsInRoom, ArrayList<MyMessage> listMessage) {
		this.roomName = RoomName;
		this.listFriendsInRoom = listFriendsInRoom;
		this.listMessage = listMessage;
	}
	
	
	public MyRoom(String roomName2) {
		// TODO Auto-generated constructor stub
		this.roomName = roomName2;
		this.listFriendsInRoom = new HashMap<String, MyFriend>();
		this.listMessage = new  ArrayList<MyMessage>();
		
	}

	public ArrayList<MyMessage> getListMessage() {
		return listMessage;
	}
	public void setListMessage(ArrayList<MyMessage> listMessage) {
		this.listMessage = listMessage;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		String data = this.roomName + "";
		
		for(Entry<String, MyFriend> entry : this.listFriendsInRoom.entrySet())
		{
			MyFriend fr = entry.getValue();
			data += "," + fr.toString();
		}

		return data;
	}

	public void sendMessageAllFriend(IController iController, MyMessage myMessage) {
		// TODO Auto-generated method stub
		this.listMessage.add(myMessage);
		writeMessageToDataBase(myMessage);
		for(Entry<String, MyFriend> entry : this.listFriendsInRoom.entrySet())
		{
			MyFriend fr = entry.getValue();
			if(!myMessage.getmUserName().equals(fr.mUserName))
			{
				iController.sendMessageToMember(fr.mUserName, myMessage,roomName);
			}
		}
		
	}

	private void writeMessageToDataBase(MyMessage myMessage) {
		// TODO Auto-generated method stub
		
	}

	public void addFriend(MyFriend myFriend2) {
		// TODO Auto-generated method stub
		this.listFriendsInRoom.put(myFriend2.getmUserName(), myFriend2);
		
	}

	public String ToStringListMessage() {
		// TODO Auto-generated method stub
		String data = "";
		for(MyMessage mes: this.listMessage)
		{
			data += mes.toString() + ",";
		}
		
		return data;
	}

	public void sendFileAllFriend(IController iController, MyMessage message, String roomName2, String fileName, Integer filsize,
			byte[] dataXXX) {
		// TODO Auto-generated method stub
		this.listMessage.add(message);
		writeMessageToDataBase(message);
		for(Entry<String, MyFriend> entry : this.listFriendsInRoom.entrySet())
		{
			MyFriend fr = entry.getValue();
			if(!message.getmUserName().equals(fr.mUserName))
			{
				iController.sendFileToMember(fr.mUserName, message,roomName2,fileName,filsize,dataXXX);
			}
		}
	}
	
}
