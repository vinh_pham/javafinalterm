package com.ptvinh.model;

public class MyMessage {

	private String mContent;
	private String mUserName;

	public String getmUserName() {
		return mUserName;
	}

	public void setmUserName(String mUserName) {
		this.mUserName = mUserName;
	}

	public MyMessage(String mUserName, String mContent) {
		this.mContent = mContent;
		this.mUserName = mUserName;
	}

	public String getmContent() {
		return mContent;
	}

	public void setmContent(String mContent) {
		this.mContent = mContent;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		
		return mUserName + "-" + getmContent();
	}
	
}
