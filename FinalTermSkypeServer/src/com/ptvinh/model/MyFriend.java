package com.ptvinh.model;

public class MyFriend {
	String mUserName;
	boolean mStatus;
	
	public MyFriend(String userName, boolean status) {
		// TODO Auto-generated constructor stub
		this.mUserName = userName;
		this.mStatus = status;
	}
	public String getmUserName() {
		return mUserName;
	}
	public void setmUserName(String mUserName) {
		this.mUserName = mUserName;
	}
	public boolean ismStatus() {
		return mStatus;
	}
	public void setmStatus(boolean mStatus) {
		this.mStatus = mStatus;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		
		return this.mUserName + "-" + this.ismStatus();
	}
	
}
