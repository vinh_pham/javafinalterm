package com.ptvinh.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.ptvinh.model.Connector;
import com.ptvinh.model.DataAccess;
import com.ptvinh.model.MyFriend;
import com.ptvinh.model.MyMember;
import com.ptvinh.model.MyMessage;
import com.ptvinh.model.MyRoom;
import com.ptvinh.view.IServerView;
import com.ptvinh.view.MainServer;

public class ServerController implements IController {

	private DataAccess dataAccess;
	private HashMap<String, MyMember> mapMembers;
	private ArrayList<Connector> listSocket;
	private static final String KEY_VERIFYAC = "VAC";
	private static final String KEY_CREATEYAC = "CAC";

	
	private String line;
	private IServerView iMainView;
	

	public ServerController(IServerView iServerView) {
		// TODO Auto-generated constructor stub
		this.iMainView = iServerView;
		this.dataAccess = new DataAccess();
		this.listSocket = new ArrayList();
	}

	public void GetDataMember()
	{
		this.mapMembers = this.dataAccess.GetDataMembersFromDatabase();
	}
	

	public void addConnector(Socket newSocketClient) {
		// TODO Auto-generated method stub
		
		Connector conn = new Connector(newSocketClient, this);
		listSocket.add(conn);

		conn.listenVerifyAccount();
		
		
	}

	@Override
	public void sendMessagetoController(String line) {
		// TODO Auto-generated method stub
		this.iMainView.updateGUI(line);
		
	}

	@Override
	public void sendAccountInfor(Connector conn, byte[] b) {
		// TODO Auto-generated method stub
		if(checkFrist3Bytes(b[0],b[1],b[2]).equals(KEY_VERIFYAC))
    	{
			String userName = getUserName(b);
			String passWord = getPassword(b);
			this.sendMessagetoController(new String(b));
			this.sendMessagetoController(userName + ".....");
    		if(checkAccount(userName, passWord))
    		{
    			this.sendMessagetoController(userName + " login success !");
    			
    			MyMember mMember = mapMembers.get(userName);
    			
    			mMember.setIController(this);
    			mMember.setConn(conn);
    			
    			mMember.setStatus(true);
    			
    			mMember.sendAnyData(); // send list friend ve client
    			mMember.listenDataFromClient();
    		}
    		else
    		{
    			conn.loginFailed();
    			this.sendMessagetoController(conn.getNameSocket() + " login failed !");
    			conn.listenVerifyAccount();
    		}
    	}else if(checkFrist3Bytes(b[0],b[1],b[2]).equals(KEY_CREATEYAC))
    	{
    		String userName = getUserName(b);
			String passWord = getPassword(b);
			
			this.sendMessagetoController("Create Account");
			this.sendMessagetoController(new String(b));
			this.sendMessagetoController(userName + ".....");
			
			if(IsExitsMember(userName))
			{
				conn.createMemberFailed();
				this.sendMessagetoController(userName + "....." + "create account failed");
    			conn.listenVerifyAccount();
				return;
			}
			MyMember userx = new MyMember(userName, passWord, true);
			
			userx.setIController(this);
			userx.setConn(conn);
			userx.listenDataFromClient();
			
			
			conn.createMemberSuccess();
			
			this.mapMembers.put(userx.getUserName(), userx);
			
			//this.dataAccess.UserRegister(userx); //update database
			
    	}
		
	}

	private boolean checkAccount(String userName, String password) {
		// TODO Auto-generated method stub
		/// kiem tra xem trong he thong co username nao da dang nhap chua
		
		if(IsExitsMember(userName))
		{
			MyMember member = this.mapMembers.get(userName);
			if(member.isStatus())
				return false;
			return true;
		}
		return false;
	}

	private String getUserName(byte[] b) {
		String byteToStr = new String(b);
		 
	  	byteToStr = byteToStr.substring(3);
	  	
	  	String[] result = byteToStr.split("/");
	  	 
	  	return result[0];
	}

	private String getPassword(byte[] b) {
		
		String byteToStr = new String(b);
	  	byteToStr = byteToStr.substring(3);
	  	
	  	String[] result = byteToStr.split("/");
	  	 
	  	return result[1];
	}

	private String checkFrist3Bytes(byte a, byte b, byte c) {
		// TODO Auto-generated method stub
		byte[] re = new byte[3];
		re[0] = a;
		re[1] = b;
		re[2] = c;
		String sre = new String(re);
		
		return sre;
	}

	@Override
	public void sendMessageToMember(String mUserName, MyMessage myMessage, String RoomName) {
		// TODO Auto-generated method stub
		if(IsUserName(mUserName))
		{
			MyMember member = this.mapMembers.get(mUserName);
			member.sendMessageToClient(RoomName,myMessage);
		}
	}

	private boolean IsUserName(String mUserName) {
		// TODO Auto-generated method stub
		return this.mapMembers.containsKey(mUserName) ? true:false;
	}

	@Override
	public void getAllListMembers(String userName) {
		// TODO Auto-generated method stub
		String dataMember = ToStringListMember(userName);
		MyMember member = this.mapMembers.get(userName);
		member.sendListAvaiableFriends(dataMember);
		
	}

	private String ToStringListMember(String userName) {
		// TODO Auto-generated method stub
		String result = "";
		
		for(Entry<String, MyMember> entry : this.mapMembers.entrySet()) {
		    String key = entry.getKey();
		    MyMember value = entry.getValue();
		    if(!value.getUserName().equals(userName))
		    	result += value.getUserName() +"-" + value.isStatus() + "/";
		}
		return result;
	}

	@Override
	public void sendMessageImOnlineorOffline(boolean onoroff, String muserName) {
		// TODO Auto-generated method stub
		if(IsExitsMember(muserName))
		{
			/*MyMember member = this.mapMembers.get(UserNameFriend);
			member.sendToClientHaveFriendOnline(muserName);*/
			for(Entry<String, MyMember> entry: this.mapMembers.entrySet())
			{
				MyMember member = entry.getValue();
				if(member.isStatus() && !member.getUserName().equals(muserName))
				{
					if(onoroff)
						member.sendToClientHaveFriendOnline(muserName);
					else 
						member.sendToClientHaveFriendOffline(muserName);
				}
			}
		}
	}

	private boolean IsExitsMember(String userNameFriend) {
		// TODO Auto-generated method stub
		return this.mapMembers.containsKey(userNameFriend)?true:false;
	}

	@Override
	public void setTrueorFalseInAllFriends(String mUserName, String userNamefr, boolean b) {
		// TODO Auto-generated method stub
		if(IsExitsMember(mUserName))
		{
			MyMember member = this.mapMembers.get(mUserName);
			member.setTrueorFalseForFriend(userNamefr, b);
		}
	}

	@Override
	public void addNewFriend(String userName, String userNameNewFriend) {
		// TODO Auto-generated method stub
		MyMember mem1 = this.mapMembers.get(userName);
		MyMember mem2 = this.mapMembers.get(userNameNewFriend);
		
		mem1.addFriend(mem2);
		mem2.addFriend(mem1);
		
		
		/*this.dataAccess.AddFriend(mem1.getUserName(), mem2.getUserName());
		this.dataAccess.AddFriend(mem2.getUserName(), mem1.getUserName());*/

		
		mem1.sendToClientAddingNewFriendSuccess(mem2.getUserName());
		mem2.sendToClientAddingNewFriendSuccess(mem1.getUserName());
	}

	@Override
	public void addRoomToMember(String creator, String userFriend, MyRoom room) {
		// TODO Auto-generated method stub
		MyMember mem1 = this.mapMembers.get(userFriend);
		mem1.addRoom(room);
		room.addFriend(new MyFriend(mem1.getUserName(), mem1.isStatus()));
		mem1.sendListRoom();
		
		MyMember mem2 = this.mapMembers.get(creator);
		mem2.sendListRoom();
	}

	@Override
	public void checkRoomNewName(String username, MyRoom room, String newName) {
		// TODO Auto-generated method stub
		String oldName = room.getRoomName();
		for(Entry<String, MyFriend> entry: room.getListFriendsInRoom().entrySet())
		{
			MyFriend fr = entry.getValue();
			
			MyMember member = this.mapMembers.get(fr.getmUserName());
			
			if(member.checkRoomNewName(newName))
			{
				// báo lỗi ko cho thay đổi name
				MyMember member2 = this.mapMembers.get(username);
				member2.notifyRoomNewNameFailed();
				return;
			}
		}
		// broadcast new name cho member
		for(Entry<String, MyFriend> entry: room.getListFriendsInRoom().entrySet())
		{
			MyFriend fr = entry.getValue();
			
			MyMember member = this.mapMembers.get(fr.getmUserName());
			if(member.isStatus())
				member.notifyRoomNewNameSuccess(oldName,newName);
			member.changeRoomNewName(oldName, newName);
			
		}
	}

	@Override
	public void sendFileToMember(String mUserName, MyMessage message, String roomName2, String fileName,
			Integer filsize, byte[] dataXXX) {
		// TODO Auto-generated method stub
		
		if(IsUserName(mUserName))
		{
			MyMember member = this.mapMembers.get(mUserName);
			member.sendFileToClient(roomName2,message,fileName, filsize, dataXXX);
		}
	}
}
