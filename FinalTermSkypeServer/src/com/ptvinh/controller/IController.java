package com.ptvinh.controller;

import com.ptvinh.model.Connector;
import com.ptvinh.model.MyMessage;
import com.ptvinh.model.MyRoom;

public interface IController {

	void sendMessagetoController(String line);

	void sendAccountInfor(Connector conn, byte[] b);

	void sendMessageToMember(String mUserName, MyMessage myMessage, String roomName);

	void getAllListMembers(String userName);


	void setTrueorFalseInAllFriends(String mUserName, String userName, boolean b);

	void addNewFriend(String userName, String userNameNewFriend);

	void addRoomToMember(String userNameCreator, String userFriend2, MyRoom room);

	void checkRoomNewName(String userName, MyRoom room, String newName);

	void sendMessageImOnlineorOffline(boolean onoroff, String userName);

	void sendFileToMember(String mUserName, MyMessage message, String roomName2, String fileName, Integer filsize,
			byte[] dataXXX);


	

}
